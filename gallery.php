<script type="text/javascript">
<!--
$(document).ready(function(){
	$("#catsList li").click(function(){
		if ($(this).hasClass("active")) return;
		$("#catsList li").removeClass("active");
		$(this).addClass("active");
		var cat=$(this).attr("id");
		cat=cat.split("_")[0];
		$(".imagesCategory").children(".catLabel").hide();
		$(".imagesCategory").children("ul").hide();
		$("#"+cat).find(".catLabel").show();
		$("#"+cat).find("ul").show();
	});
});
//-->
</script>
<?php
   include './phpThumb/phpthumb.class.php';
   function listPicsFromCat($catname){
   		$phpThumb = new phpThumb();
	   	$output="";
	   	$gallery = glob(dirname(__FILE__)."/images/custom.design/".$catname."/*.jpg");
	   	foreach ($gallery as $imagefname) {
	   		$basename = basename($imagefname, ".jpg");
	   		$src='images/custom.design/'.$catname.'/'.$basename.'.jpg';
	   		$dst='images/custom.design/'.$catname.'/thumbs/'.$basename.'_thumb.jpg';
	   		if (!file_exists('./'.$dst)){
		   		$phpThumb->resetObject();  		
		   		$phpThumb->setParameter('w', 180);
		   		$phpThumb->setParameter('h', 120);
		   		$phpThumb->setParameter('q', 100);
		   		$phpThumb->setParameter('zc', 1);
		   		$phpThumb->setSourceFilename(dirname(__FILE__).'/images/custom.design/'.$catname.'/'.$basename.'.jpg');
		   		$phpThumb->GenerateThumbnail();
		   		$phpThumb->renderToFile(dirname(__FILE__).'/images/custom.design/'.$catname.'/thumbs/'.$basename.'_thumb.jpg');
	   		}
	   		$output.="<li><img src='".$dst."'/></li>";
	   		$phpThumb->purgeTempFiles();
	   	}
	   	return $output;
   }
?>
<?php 
	$phpThumb = new phpThumb();
	$phpThumb->setParameter('w', 180);
	$phpThumb->setParameter('h', 120);
?>
<form action="./" method="post">
	<input type="hidden" name="action" value="choice_typecard" />
	<input type="hidden" name="isback" value="0" />
	<div id="allpics" class="card-images-grouper">
		<div id="catsList">
			<p class="catLabel">Показать</p>
			<ul>
				<li class="active" id="allpics_cat">все</li>
				<li id="animals_cat">животные</li>
				<li id="retro_cat">ретро и сепия</li>
				<li id="nature_cat">природа</li>
				<li id="trips_cat">путешествия</li>
				<li id="art_cat">искусство</li>
				<li id="textures_cat">текстуры</li>
			</ul>
		</div>
		<div class="imagesCategory" id="animals">
			<p class="catLabel">Животные</p>
			<ul><? echo listPicsFromCat('animals');?></ul>
		</div>
		<div class="imagesCategory" id="retro">
			<p class="catLabel">Ретро и сепия</p>
			<ul><? echo listPicsFromCat('retro');?></ul>
		</div>
		<div class="imagesCategory" id="nature">
			<p class="catLabel">Природа</p>
			<ul><? echo listPicsFromCat('nature');?></ul>
		</div>
		<div class="imagesCategory" id="trips">
			<p class="catLabel">Путешествия</p>
			<ul><? echo listPicsFromCat('trips');?></ul>
		</div>
		<div class="imagesCategory" id="art">
			<p class="catLabel">Искусство</p>
			<ul><? echo listPicsFromCat('art');?></ul>
		</div>
		<div class="imagesCategory" id="textures">
			<p class="catLabel">Текстуры</p>
			<ul><? echo listPicsFromCat('textures');?></ul>
		</div>
	</div>
	<div class="clear"></div>
	<input type="submit" name="submit" id="goback" value="Назад" />
	<input type="submit" name="submit" id="submitit" value="Продолжить" />			
</form>