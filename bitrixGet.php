<?php
  
include_once 'bitrixDb.php';
include_once 'db_shared.php';
error_reporting(E_ERROR);
ini_set('error_reporting', 0);
ini_set("display_errors",0);
ini_set("html_errors",0);
//ОФИСЫ БАНКА 
$IBLOCK_ID=25;
$IBLOCK_ID2 = "";
$SECTION_ID=95;
//НЕ_ИНТЕРЕСУЮЩИЕ_РЕГИОНЫ
$EXCL_SECTION_ID= array(96,113);
//РЕГИОН_ОБЛАСТЬ
$ROVREG_SECTION_ID=101;
//МОСКОВСКАЯ_ОБЛАСТЬ
$MSK_SECTION_ID=286;
//РОСТОВ_PROPERTY_115
$ROV_CITY_ID=24;
//МОСКВА_PROPERTY_115
$MSK_CITY_ID=38;
//Карты
$IND_LOAN_PROP=122;
//переименование для областей
//в базе -"в Ростовской области"
$datalists['region_rename'] = array(
	'101'=> 'Ростовская область',
	'110'=> 'Ставропольский край',		
	'108'=> 'Краснодарский край',
	'111'=> 'Волгоградская область',
	'286'=> 'Московская область'
);

$SQL_GET_REGIONS="SELECT 
					`b_iblock_section`.`NAME` AS name,
					`b_iblock_section`.`ID` AS id,
					`b_iblock_section`.`SORT` AS sort
					FROM  `b_iblock_section` 
					WHERE `b_iblock_section`.`IBLOCK_ID`='{$IBLOCK_ID}' AND `b_iblock_section`.`IBLOCK_SECTION_ID`='%1\$d' AND `b_iblock_section`.`ACTIVE`='Y'
					ORDER BY `b_iblock_section`.`SORT`";


$SQL_GET_CITIES="SELECT
				`b_iblock_property_enum`.`VALUE` AS name,
				`b_iblock_element_prop_s25`.`PROPERTY_92` AS id,
				`b_iblock_element`.`SORT` AS sort
				FROM     `b_iblock_element`
				LEFT JOIN  `b_iblock_section_element`   ON `b_iblock_section_element`.`IBLOCK_ELEMENT_ID`=`b_iblock_element`.`ID`
				LEFT JOIN  `b_iblock_section`           ON `b_iblock_section`.`ID`=`b_iblock_section_element`.`IBLOCK_SECTION_ID`
				LEFT JOIN  `b_iblock_element_prop_s25`  ON `b_iblock_element_prop_s25`.`IBLOCK_ELEMENT_ID`=`b_iblock_element`.`ID`
				LEFT JOIN  `b_iblock_property_enum`     ON `b_iblock_element_prop_s25`.`PROPERTY_92`=`b_iblock_property_enum`.`ID`
				LEFT JOIN `b_iblock_element_prop_m25` ON  `b_iblock_element_prop_m25`.`IBLOCK_ELEMENT_ID` =  `b_iblock_element`.`ID`
				WHERE `b_iblock_element`.`IBLOCK_ID`='{$IBLOCK_ID}' AND `b_iblock_element`.`ACTIVE` = 'Y'
				AND `b_iblock_section`.`ID` in (select `id` from `b_iblock_section` where `iblock_section_id`='%1\$d') and `b_iblock_element_prop_m25`.`IBLOCK_PROPERTY_ID` = '100'
				and `b_iblock_element_prop_m25`.`VALUE` =  '{$IND_LOAN_PROP}'
				GROUP BY id
				ORDER BY `b_iblock_element`.`SORT`";


/*$SQL_GET_OFFICES="SELECT
					`b_iblock_element_prop_s27`.`PROPERTY_193` AS id,
					`b_iblock_element`.`NAME` AS name,  
					`b_iblock_element_prop_s27`.`PROPERTY_101` AS address,   
					`b_iblock_property_enum`.`VALUE` AS city,					   
					`b_iblock_element_prop_s27`.`PROPERTY_110` AS usl
					FROM  `b_iblock_element` 
					LEFT JOIN  `b_iblock_element_prop_s27` ON  `b_iblock_element_prop_s27`.`IBLOCK_ELEMENT_ID` =  `b_iblock_element`.`ID` 
					LEFT JOIN  `b_iblock_property_enum` ON  `b_iblock_element_prop_s27`.`PROPERTY_115` =  `b_iblock_property_enum`.`ID` 
					WHERE  `b_iblock_element`.`IBLOCK_ID` =  '{$IBLOCK_ID}'
					AND  `b_iblock_element_prop_s27`.`PROPERTY_115` =  '%1\$d'
					ORDER BY  `b_iblock_element_prop_s27`.`PROPERTY_193` DESC";*/

$SQL_GET_OFFICES="SELECT `b_iblock_element`.`ID` AS id,
					`b_iblock_element`.`NAME` AS name,  
					`b_iblock_element_prop_s25`.`PROPERTY_93` AS address,   
					`b_iblock_property_enum`.`VALUE` AS city					   
FROM  `b_iblock_element`
LEFT JOIN  `b_iblock_element_prop_s25` ON  `b_iblock_element_prop_s25`.`IBLOCK_ELEMENT_ID` =  `b_iblock_element`.`ID`
LEFT JOIN  `b_iblock_property_enum` ON  `b_iblock_element_prop_s25`.`PROPERTY_92` =  `b_iblock_property_enum`.`ID`
LEFT JOIN `b_iblock_element_prop_m25` ON  `b_iblock_element_prop_m25`.`IBLOCK_ELEMENT_ID` =  `b_iblock_element`.`ID` 
WHERE  `b_iblock_element`.`IBLOCK_ID` =   '{$IBLOCK_ID}' AND `b_iblock_element`.`ACTIVE` = 'Y'
					AND  `b_iblock_element_prop_s25`.`PROPERTY_92` =  '%1\$d' and `b_iblock_element_prop_m25`.`IBLOCK_PROPERTY_ID` = '100'
and `b_iblock_element_prop_m25`.`VALUE` =  '{$IND_LOAN_PROP}'
					ORDER BY `b_iblock_element`.`NAME`";

$SQL_GET_OFFICE_COORDS="SELECT `b_iblock_element`.`ID` AS id,
`b_iblock_element_prop_s25`.`PROPERTY_104` AS x,
`b_iblock_element_prop_s25`.`PROPERTY_105` AS y
FROM  `b_iblock_element`
LEFT JOIN  `b_iblock_element_prop_s25` ON  `b_iblock_element_prop_s25`.`IBLOCK_ELEMENT_ID` =  `b_iblock_element`.`ID`
LEFT JOIN `b_iblock_element_prop_m25` ON  `b_iblock_element_prop_m25`.`IBLOCK_ELEMENT_ID` =  `b_iblock_element`.`ID` 
WHERE  `b_iblock_element`.`IBLOCK_ID` =   '{$IBLOCK_ID}' AND `b_iblock_element`.`ACTIVE` = 'Y'
AND  `b_iblock_element`.`ID` =  '%1\$d' and `b_iblock_element_prop_m25`.`IBLOCK_PROPERTY_ID` = '100'
and `b_iblock_element_prop_m25`.`VALUE` =  '{$IND_LOAN_PROP}'
ORDER BY `b_iblock_element`.`NAME`";

$SQL_GET_OFFICESPhone="SELECT `b_iblock_element_prop_s25`.`PROPERTY_107` AS id,
					`b_iblock_element`.`NAME` AS name,  
					`b_iblock_element_prop_s25`.`PROPERTY_93` AS address,   
					`b_iblock_element_prop_s25`.`PROPERTY_97` AS phone, 
					`b_iblock_property_enum`.`VALUE` AS city					   
                    FROM  `b_iblock_element`
                            LEFT JOIN  `b_iblock_element_prop_s25` ON  `b_iblock_element_prop_s25`.`IBLOCK_ELEMENT_ID` =  `b_iblock_element`.`ID`
                            LEFT JOIN  `b_iblock_property_enum` ON  `b_iblock_element_prop_s25`.`PROPERTY_92` =  `b_iblock_property_enum`.`ID`
                            LEFT JOIN `b_iblock_element_prop_m25` ON  `b_iblock_element_prop_m25`.`IBLOCK_ELEMENT_ID` =  `b_iblock_element`.`ID` 
                    WHERE  `b_iblock_element`.`IBLOCK_ID` =   '{$IBLOCK_ID}'
                            AND `b_iblock_element`.`ACTIVE` = 'Y'
                            AND  `b_iblock_element_prop_s25`.`PROPERTY_92` =  '%1\$d' 
                            and `b_iblock_element_prop_m25`.`IBLOCK_PROPERTY_ID` = '100'
                            and `b_iblock_element_prop_m25`.`VALUE` =  '{$IND_LOAN_PROP}'
                            and `b_iblock_element_prop_s25`.`PROPERTY_107` = '%2\$d'
                    ORDER BY `b_iblock_element`.`NAME`";


	function getRegions($region){
		global $SQL_GET_REGIONS, $SECTION_ID, $EXCL_SECTION_ID, $datalists;
		$qResult = mysql_query(sprintf($SQL_GET_REGIONS,$region))or die (mysql_error());
		while ($row = mysql_fetch_assoc($qResult)) {
			if(!in_array($row["id"],$EXCL_SECTION_ID))
				$result[$row["id"]] = isset($datalists['region_rename'][$row["id"]])?
											$datalists["region_rename"][$row["id"]]:
											/*iconv('windows-1251', 'UTF-8', */$row["name"]/*)*/;
		}
		return $result;
	}
	
	function getCities($region){
		global $SQL_GET_CITIES, $ROV_CITY_ID, $ROVREG_SECTION_ID, $IND_LOAN_PROP,$MSK_CITY_ID,$MSK_SECTION_ID;		
		$qResult = mysql_query(sprintf($SQL_GET_CITIES,$region,"%\"{$IND_LOAN_PROP}\"%"))or die (mysql_error());
		if($region==$ROVREG_SECTION_ID)
		{
			//Ростов - отдельный регион, добавляем код города вручную
			$result[$ROV_CITY_ID]="г. Ростов-на-Дону";
		}	
		if($region==$MSK_SECTION_ID)
		{ 
			
			//Москва - отдельный регион, добавляем код города вручную
			$result[$MSK_CITY_ID]="г. Москва";
		}	
		
		while ($row = mysql_fetch_assoc($qResult)) {
			$result[$row["id"]] = /*iconv('windows-1251', 'UTF-8',*/$row["name"]/*)*/;
		}
		return $result;
	}
	
	function getOffices($city){
		global $SQL_GET_OFFICES, $IND_LOAN_PROP;	
		$qResult = mysql_query(sprintf($SQL_GET_OFFICES,$city))or die (mysql_error());
		while ($row = mysql_fetch_assoc($qResult)) {
			//$usl=unserialize($row["usl"]);
			//if(in_array($IND_LOAN_PROP,$usl["VALUE"]))
				$result[$row["id"]] = /*iconv('windows-1251', 'UTF-8',*/$row["name"]." ".$row["address"].""/*)*/;
		}
		//
		if(!isset($result))
			$result[""]="-Не доступен-";
		return $result;
	}	
	function getOfficeCoords($office){
		global $SQL_GET_OFFICE_COORDS, $IND_LOAN_PROP;
		$qResult = mysql_query(sprintf($SQL_GET_OFFICE_COORDS,$office))or die (mysql_error());
		$row = mysql_fetch_assoc($qResult);
		$result = $row["x"]."_".$row["y"];
		return $result;
	}
        function getOfficesPhone($city, $office){
		global $SQL_GET_OFFICESPhone, $IND_LOAN_PROP;	
		$qResult = mysql_query(sprintf($SQL_GET_OFFICESPhone,$city, $office))or die (mysql_error());
		while ($row = mysql_fetch_assoc($qResult)) {
			//$usl=unserialize($row["usl"]);
			//if(in_array($IND_LOAN_PROP,$usl["VALUE"]))
				$result["phone"] = $row["phone"];
                                $result["city"] = $row["city"];
		}
		//
		if(!isset($result))
			$result[""]="-Не доступен-";
		return $result;
	}
	
	
//	function jsonit($get){		
//		foreach($get as $key=>$value){
//			$result[] = array("value" => $key, "text" => $value);
//		}
//		return json_encode($result);
//	}
	
	//var_dump(getCities(284));
//print_r($_SERVER);	 
        if(  isset($_SERVER['HTTP_X_REQUESTED_WITH'])  ){
	if(  $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
		if(!isset($_GET['call']) ) return;
                $call=$_GET['call'];		
		switch($call){
			case 'getRegions':
				echo jsonit(getRegions($SECTION_ID));
				break;
			case 'getCities':
				$region = (int)mysql_real_escape_string($_GET['param']);
				if($region>0) echo jsonit(getCities($region));
				break;
			case 'getOffices':
				$city = (int)mysql_real_escape_string($_GET['param']);
				if($city>0) echo jsonit(getOffices($city));
				break;
			case 'getOfficeCoords':
				$office = (int)mysql_real_escape_string($_GET['param']);
				if($office>0) echo getOfficeCoords($office);
				break;
		}
	}
        }
?>