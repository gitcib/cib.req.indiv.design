<?
error_reporting(E_ERROR);
ini_set('error_reporting', 0);
ini_set("display_errors",0);
ini_set("html_errors",0);
	header('Content-type: text/html; charset=utf-8');
    require_once 'path.php';
	ini_set('session.gc_maxlifetime', 60*60*24);
	ini_set('session.save_path', '/mnt/big/apache/htdocs.cib.data/indiv.design/htdocscib_sessions/');
	session_start();
	$gal_preview=0;
	if (isset($_GET["gal_preview"]) && $_GET["gal_preview"]==1)
		$gal_preview=1;
	if ($_POST) {
		foreach ($_POST as $key => $value) {
			$_SESSION['indiv.design'][htmlspecialchars($key)] = htmlspecialchars($value);
		}
	}
	//var_dump($_SESSION);
	$action = (isset($_SESSION['indiv.design']['action']))?$_SESSION['indiv.design']['action']:"";
	$prev_action = "";
	if (!isset($_SESSION['indiv.design']['actionstack']) || empty($action)){
		$_SESSION['indiv.design']['actionstack']=array();
	}
	if (!empty($action)) {
		if (isset($_SESSION['indiv.design']["isback"]) && $_SESSION['indiv.design']["isback"] == "1") {
			$cur_action = array_pop($_SESSION['indiv.design']['actionstack']);
			if (count($_SESSION['indiv.design']['actionstack']) > 1) {
				$prev_action = $_SESSION['indiv.design']['actionstack'][count($_SESSION['indiv.design']['actionstack'])-2];
			}
		} else {
			if (!empty($_SESSION['indiv.design']['actionstack'])) {
				$prev_action = $_SESSION['indiv.design']['actionstack'][count($_SESSION['indiv.design']['actionstack'])-1];
			}
			if ($prev_action != $action) {
				array_push($_SESSION['indiv.design']['actionstack'], $action);
			}
		}
	}
	if(empty($_SESSION)){
		echo "Время жизни сессии истекло. <a href='clear.gotostart.php'>Начать заполнение сначала</a>";
		die();
	}
	//echo $action;
	//echo var_dump($_SESSION['indiv.design']);
	//echo "<br/>";
	/*print_r($_SESSION['indiv.design']);*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Заявка на карту с индивидуальным дизайном</title>
	<? if (($action == "personal-data-fill-1")) { ?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<? } 
	else{?>
		<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<?}?>
	<? if (($action == "custom-design-selector")) { ?>
			<link rel="stylesheet" type="text/css" href="css/imgareaselect-default.css" />
			<script type="text/javascript" src="js/jquery.imgareaselect.min.js"></script>
	<? } ?>
	<link rel="stylesheet" href="/css/layout.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/form.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<? if (($action == "agree-personal-data-choice") || ($action == "pack_choice") || ($action == "sa_pack_choice")) { ?>


			

	<? } ?>
	<? if ($action != "personal-data-fill-1") { ?>
		<script src="js/jquery.maskedinput-1.3.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/jquery.validate.js"></script>
		<script type="text/javascript" src="js/validate.js"></script>
		<script type="text/javascript" src="js/niceCheck.js"></script>
		<script type="text/javascript" src="js/niceRadio.js"></script>
		<script type="text/javascript" src="js/document.js"></script>
	<? } ?>
	<script type="text/javascript" src="https://vk.com/js/api/share.js?90" charset="windows-1251"></script>
</head>
<body>
    <h1>Заявка на выпуск карты с индивидуальным дизайном</h1>
    <?    	
    	if ($gal_preview) require_once $PATH_script . '/gallery.php';	
    	else switch ($action) {
			case "choice_typecard":
				require_once $PATH_script . '/pack.choice.php';	
				break;
    		case "pack_choice":
    			require_once $PATH_script . '/custom.design.selector.php';
    			break;
    		case "personal-data-form-1":
    			require_once $PATH_script . '/save.request.php';
    			break;
    		case "custom-design-selector":
    			require_once $PATH_script . '/custom.design.constructor.php';
    			break;
    		case "custom-design-constructor":
    			require_once $PATH_script . '/personal.data.form.1.php';
    			break;
    		default:
    			require_once $PATH_script . '/choice_typecard.php';
    			break;
    	}
		if (!empty($action) && ($action!="save-request")) { ?>
			<a class="button" href="clear.gotostart.php" id="gotostart">Начать заново</a>
		<? } ?>

	</body>
</html>
