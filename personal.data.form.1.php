<script type="text/javascript">
<!--
jQuery(function($){
	 $("#personal-data-1-depCode").mask("999-999", {autoclear: false});
	 $("input#personal-data-1-cellPhoneNum").mask("+79999999999", {autoclear: false});
	 $("#personal-data-1-4num").mask("9999", {autoclear: false});
	 $("#personal-data-1-4num2").mask("9999", {autoclear: false});
	 $("#personal-data-1-passportSeries").mask("99 99", {autoclear: false});
	 $("#personal-data-1-passportNumber").mask("999999", {autoclear: false});
	 $("#personal-data-1-inn").mask("999999999999", {autoclear: false});
});

$(document).ready(function(){
	$("input[name='personal-data-1-emailEveryMonth']").change(function(){
		if ($(this).is(":checked") && $(this).val()=="yes")
			$("#personalDataEmail").show();
		else {
			$("#personalDataEmail").hide();
		}
	}).change();
	
	$('input#goback').click(function(){
		$('input[name=action]').val($('input[name=prev_action]').val());
		$('input[name=isback]').val("1");
		$('#personal-data-form-1').submit();
	});

	$('a.readmore').click(function(){
		var linkId = $(this).attr('id');
		var numId = linkId.replace(/input/g,'');
		var oOffset = $(this).offset();
		$('div#info'+numId).fadeIn('slow');
	});
	$('div.readmore_hidden a.readmore_hidden_close').click(function(){
		$(this).closest('div.readmore_hidden').fadeOut('slow');
	});

	$('#region').change(function() {
		$("#off_reg").val($("#region  option[value='"+$("#region").val()+"']").text());
		popFrom($(this), $('#city'), null, 'bitrixGet', 'getCities');
		$(this).ajaxComplete(function(){
			$('#city').change();
			$(this).unbind('ajaxComplete');
		});
    });
	$('#city').change(function() {	
		$("#off_city").val($("#city  option[value='"+$("#city").val()+"']").text());
		popFrom($(this), $('#code_orgunit'), null, 'bitrixGet', 'getOffices');		
		$(this).ajaxComplete(function(){
			$('#code_orgunit').change();
			$(this).unbind('ajaxComplete');
		});
    });
	$('#code_orgunit').change(function() {		
  		$("#office").remove();
  		$("#code_orgunit_div").after("<p id='office'  style='font-size:12px;margin:-15px 0 -5px 300px;'><a target='_blank' href='/ru/branches/offices/#"+$("#code_orgunit").val()+"'>посмотреть на карте</a></p>");
		$("#off_addr").val($("#code_orgunit option[value='"+$("#code_orgunit").val()+"']").text());
    });
	
    
    if($('#region').children().length==0) {
		fill($('#region'), 'bitrixGet', 'getRegions');
	}
    else{
        $('#region').change();
        if ($('#city').children().length!=0) {
             $('#city').change();
        }
    }

	$("#code_orgunit").change(function(){
		$("#off_addr").val($("#code_orgunit option[value='"+$("#code_orgunit").val()+"']").text());
	});
	
	$('form#personal-data-form-1').submit(function(){
		if ( typeof(parent.yaCounter4814476) != "undefined" ){
			parent.yaCounter4814476.reachGoal('design_form');
		} 
		if ($('input[name=isback]').val() != "1") {
			$("#region").find('option:selected').val($("#region").find('option:selected').text());
			$("#city").find('option:selected').val($("#city").find('option:selected').text());
			$("#code_orgunit").find('option:selected').val($("#code_orgunit").find('option:selected').text());
			return true;//validator($(this).attr('id'));
		}
	});
	$("#iBank").on('click', '.ask', function(){     
	    //alert($(this).html());
	        $(this).next(".wtalb").show();
	    });
	$("#iBank").on('click', '.closeI', function(){        
	        $(this).parents(".wtalb").hide();
	});

});
function doGetCaretPosition (ctrl) {
	var CaretPos = 0;
	// IE Support
	if (document.selection) {
	ctrl.focus();
	var Sel = document.selection.createRange ();
	Sel.moveStart ('character', -ctrl.value.length);
	CaretPos = Sel.text.length;
	}
	// Firefox support
	else if (ctrl.selectionStart || ctrl.selectionStart == '0')
	CaretPos = ctrl.selectionStart;
	return (CaretPos);
} 

//-->
</script>
<form name="personal-data-form-1" id="personal-data-form-1" action="./" method="post">
	<input type="hidden" name="prev_action" value="<?php echo $prev_action; ?>" />
	<input type="hidden" name="isback" value="0" />
	<input type="hidden" name="action" value="personal-data-form-1" />
	<input type="hidden" name="off_reg" value="" id="off_reg"/>
	<input type="hidden" name="off_city" value="" id="off_city"/>
	<input type="hidden" name="off_addr" value="" id="off_addr"/>
	<fieldset id="personal">
		<legend>Персональные данные</legend>
		<div class="element">
			<label for="personal-data-1-f">Фамилия <em>*</em></label>
			<input type="text" name="clLastname" class="required cyrillic-lname l2" id="personal-data-1-f" value="<?php if ( isset($_SESSION['indiv.design']['clLastname'])) echo $_SESSION['indiv.design']['clLastname'];?>"/>
			<div style="display: none;" id="fio_hint" class="hint"><span>Укажите как в паспорте</span></div>
		</div>
		<div class="element">
			<label for="personal-data-1-i">Имя<em>*</em></label>
			<input type="text" name="clName" class="required cyrillic l2" id="personal-data-1-i" value="<?php if ( isset($_SESSION['indiv.design']['clName'])) echo $_SESSION['indiv.design']['clName'];?>"/>
			<div style="display: none;" id="fio_hint" class="hint"><span>Укажите как в паспорте</span></div>
		</div>
		<div class="element">
			<label for="personal-data-1-o">Отчество<em>*</em></label> 
			<input type="text" name="clSurname" class="required cyrillic l2" id="personal-data-1-o"  value="<?php if ( isset($_SESSION['indiv.design']['clSurname'])) echo $_SESSION['indiv.design']['clSurname'];?>"/>
			<div style="display: none;" id="fio_hint" class="hint"><span>Укажите как в паспорте</span></div>
		</div>
		<div class="element">
			<label for="personal-data-1-birthDate">Дата рождения<em>*</em></label>  
			<input type="text" name="clBirthDate" class="required date birthdate" id="personal-data-1-birthDate" value="<?php if ( isset($_SESSION['indiv.design']['clBirthDate'])) echo $_SESSION['indiv.design']['clBirthDate'];?>"/>
			<div style="display: none;" id="birth_hint" class="hint"><span>Укажите корректную дату</span></div>
		</div>
		<?php if ($_SESSION['indiv.design']['chcard']=='individual'){?>
			<div class="element">
				<label for="personal-data-1-birthPlace">Место рождения</label>  
				<input type="text" name="clBirthPlace" class="txtreq" id="personal-data-1-birthPlace" value="<?php if ( isset($_SESSION['indiv.design']['clBirthPlace'])) echo $_SESSION['indiv.design']['clBirthPlace'];?>"/>
				<div id="personal-data-1-birthPlaceError" class="hint"><span>Укажите как в паспорте</span></div>
			</div>
			<div class="element">
				<label for="personal-data-1-passportSeries">Серия паспорта<em>*</em></label>
				<input type="text" name="clPassportSeries" class="required ser-idpass pass-ser" id="personal-data-1-passportSeries" length="5" value="<?php if ( isset($_SESSION['indiv.design']['clPassportSeries'])) echo $_SESSION['indiv.design']['clPassportSeries'];?>" />
			</div>
			<div class="element">
				<label for="personal-data-1-passportNumber">Номер паспорта<em>*</em></label> 
				<input type="text" name="clPassportNum" class="required idnum pass-num" id="personal-data-1-passportNumber" length="6" value="<?php if ( isset($_SESSION['indiv.design']['clPassportNum'])) echo $_SESSION['indiv.design']['clPassportNum'];?>" />
			</div>
			<div class="element">
				<label for="personal-data-1-passportIssuePlace">Место выдачи паспорта<em>*</em></label>  
				<input type="text" name="clPassportIssuePlace" class="required nonlatin" id="personal-data-1-passportIssuePlace" value="<?php if ( isset($_SESSION['indiv.design']['clPassportIssuePlace'])) echo $_SESSION['indiv.design']['clPassportIssuePlace'];?>"  />
				<div id="personal-data-1-passportIssuePlaceError" class="hint"><span>Наименование органа, выдавшего документ</span></div>
			</div>
			<div class="element">
				<label for="personal-data-1-passportIssueDate">Дата выдачи паспорта<em>*</em></label> 
				<input type="text" name="clPassportIssueDate" class="required date" id="personal-data-1-passportIssueDate" value="<?php if ( isset($_SESSION['indiv.design']['clPassportIssueDate'])) echo $_SESSION['indiv.design']['clPassportIssueDate'];?>" />
				<div style="display: none;" id="clPassportIssueDate_hint" class="hint"><span>Укажите корректную дату</span></div>
			</div>
			<div class="element">
				<label for="personal-data-1-depCode">Код подразделения<em>*</em></label>  
				<input type="text" name="clDivisionCode" class="required idplacecode pass-kp" id="personal-data-1-depCode" value="<?php if ( isset($_SESSION['indiv.design']['clDivisionCode'])) echo $_SESSION['indiv.design']['clDivisionCode'];?>" length="7" />
				<div class="hint" id="clDivisionCode_hint" style="display: none;"><span>Укажите код подразделения, выдавшего документ</span></div>
			</div>
			<div class="element">
				<label for="personal-data-1-rfResident">Резидент РФ<em>*</em></label>
				<div class="radiogroup"  id="personal-data-1-classcard">
				    <div>
				    	<input type="radio" id="Resident_y" name="personal-data-1-rfResident" value="yes" class="required niceRadio" <?if (!isset($_SESSION['indiv.design']['personal-data-1-rfResident']) ||  $_SESSION['indiv.design']['personal-data-1-rfResident']=="yes"):?>checked="checked"<?endif?>>
				    	<label for="Resident_y">да</label>
						<input type="radio" id="Resident_n" name="personal-data-1-rfResident" value="no" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['personal-data-1-rfResident']) &&  $_SESSION['indiv.design']['personal-data-1-rfResident']=="no"):?>checked="checked"<?endif?>>
						<label for="Resident_n">нет</label>
					</div>
				</div>
			</div>
			<div class="element">
				<label for="personal-data-1-regAddress">Адрес регистрации<em>*</em></label>  
				<input type="text" name="clRegAddress" class="required txtreq" id="personal-data-1-regAddress" value="<?php if ( isset($_SESSION['indiv.design']['clRegAddress'])) echo $_SESSION['indiv.design']['clRegAddress'];?>"/>
				<div id="personal-data-1-regAddressError" class="hint"><span>Укажите как в паспорте</span></div>
			</div>
			<div class="element">
				<label for="personal-data-1-inn">ИНН</label>
				<input type="text" name="clINN" class="inn" id="personal-data-1-inn" value="<?php if ( isset($_SESSION['indiv.design']['clINN'])) echo $_SESSION['indiv.design']['clINN'];?>" length="12"/>
				<div id="personal-data-1-innError" class="hint"><span for="per_inn" generated="true" class="errored" style="display: inline;">Укажите 12 цифр</span></div>
			</div>
			<div class="element">
				<label for="personal-data-1-workPlace">Место работы<em>*</em></label> 
				<input type="text" name="personal-data-1-workPlace" id="personal-data-1-workPlace" value="<?php if ( isset($_SESSION['indiv.design']['personal-data-1-workPlace'])) echo $_SESSION['indiv.design']['personal-data-1-workPlace'];?>" />
			</div>
			<!-- <div class="element">
				<label for="personal-data-1-workAddress">Адрес места работы<em>*</em></label>  
				<input type="text" name="personal-data-1-workAddress" id="personal-data-1-workAddress" value="<?php if ( isset($_SESSION['indiv.design']['personal-data-1-workAddress'])) echo $_SESSION['indiv.design']['personal-data-1-workAddress'];?>" />
			</div>
			<div class="element">
				<label for="personal-data-1-workPosition">Должность<em>*</em></label> 
				<input type="text" name="personal-data-1-workPosition" id="personal-data-1-workPosition" value="<?php if ( isset($_SESSION['indiv.design']['personal-data-1-workPosition'])) echo $_SESSION['indiv.design']['personal-data-1-workPosition'];?>" />
			</div> 
			<div class="element">
				<label for="personal-data-1-codeWord">Кодовое слово для идентификации по телефону (девичья фамилия матери)<em>*</em></label>
				<input type="text" name="clCodeWord" class="required cyrillic" id="personal-data-1-codeWord" value="<?php if ( isset($_SESSION['indiv.design']['clCodeWord'])) echo $_SESSION['indiv.design']['clCodeWord'];?>"/>
				<div id="codeWord_hint" class="hint"><span>Только русские буквы</span></div>
			</div> -->
		<?php } ?>
		<div class="element">
			<label for="personal-data-1-cellPhoneNum">Мобильный телефон<em>*</em></label>    
			<input type="text" name="clCellPhoneNum" class="required phone" id="personal-data-1-cellPhoneNum" value="<?php if ( isset($_SESSION['indiv.design']['clCellPhoneNum'])) echo $_SESSION['indiv.design']['clCellPhoneNum'];?>" length="12" />
		</div>
	</fieldset>
	<fieldset id="personal">
		<legend>Информация о карте</legend>
		<?php if ($_SESSION['indiv.design']['chcard']=='salary_account' || $_SESSION['indiv.design']['chcard']=='indiv_addit'){?>
		<div class="element">
			<label>Первые 4 цифры номера<br>имеющейся карты<em>*</em></label>
			<div class="radiogroup"  id="personal-data-1-cardCurrency">
			    <div>
			    	<input type="radio" id="n6763" name="clFirst4numcard" value="6763" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['clFirst4numcard']) &&  $_SESSION['indiv.design']['clFirst4numcard']=="6763"):?>checked="checked"<?endif?>>
			    	<label for="n6763" style="width:45px !important;margin-left:3px;">6763</label>
			    	<input type="radio" id="n4117" name="clFirst4numcard" value="4117" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['clFirst4numcard']) &&  $_SESSION['indiv.design']['clFirst4numcard']=="4117"):?>checked="checked"<?endif?>>
			    	<label for="n4117" style="width:45px !important;margin-left:3px;">4117</label>
					<input type="radio" id="n5419" name="clFirst4numcard" value="5419" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['clFirst4numcard']) &&  $_SESSION['indiv.design']['clFirst4numcard']=="5419"):?>checked="checked"<?endif?>>
			    	<label for="n5419" style="width:45px !important;margin-left:3px;">5419</label>
			    	<input type="radio" id="n4303" name="clFirst4numcard" value="4303" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['clFirst4numcard']) &&  $_SESSION['indiv.design']['clFirst4numcard']=="4303"):?>checked="checked"<?endif?>>
			    	<label for="n4303" style="width:45px !important;margin-left:3px;">4303</label>
			    	<input type="radio" id="n5149" name="clFirst4numcard" value="5149" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['clFirst4numcard']) &&  $_SESSION['indiv.design']['clFirst4numcard']=="5149"):?>checked="checked"<?endif?>>
			    	<label for="n5149" style="width:45px !important;margin-left:3px;">5149</label>
			    	<?php if ($_SESSION['indiv.design']['chcard']!='indiv_addit'){?>
			    	<input type="radio" id="n4585" name="clFirst4numcard" value="4585" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['clFirst4numcard']) &&  $_SESSION['indiv.design']['clFirst4numcard']=="4585"):?>checked="checked"<?endif?>>
			    	<label for="n4585" style="width:45px !important;margin-left:3px;">4585</label>
			    	<?php }?>
				</div>
			</div>
		</div>
		<div class="element">
			<label>Последние 4 цифры номера<br>имеющейся карты<em>*</em></label>
			<input type="text" name="clLast4numcard" id="personal-data-1-4num2" class="required"  length="4" 
			value="<?php if ( isset($_SESSION['indiv.design']['clLast4numcard'])) echo $_SESSION['indiv.design']['clLast4numcard'];?>" />
		</div>
		<?php } else {?>
		<div class="element">
			<label>Валюта карточного счета<em>*</em></label>
			<div class="radiogroup"  id="personal-data-1-cardCurrency">
			    <div>
			    	<input type="radio" id="RUR" name="personal-data-1-cardCurrency" value="rur" class="required niceRadio" <?if (!isset($_SESSION['indiv.design']['personal-data-1-cardCurrency']) ||  $_SESSION['indiv.design']['personal-data-1-cardCurrency']=="rur"):?>checked="checked"<?endif?>>
			    	<label for="electron" style="width:65px !important;">рубли</label>
			    	<input type="radio" id="eur" name="personal-data-1-cardCurrency" value="eur" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['personal-data-1-cardCurrency']) &&  $_SESSION['indiv.design']['personal-data-1-cardCurrency']=="eur"):?>checked="checked"<?endif?>>
					<label for="classic" style="width:60px  !important;;">евро</label>
					<input type="radio" id="USD" name="personal-data-1-cardCurrency" value="usd" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['personal-data-1-cardCurrency']) &&  $_SESSION['indiv.design']['personal-data-1-cardCurrency']=="usd"):?>checked="checked"<?endif?>>
					<label for="classic" style="width:130px  !important;">доллары США</label>
					
				</div>
			</div>
		</div>
		<?php }?>
	</fieldset>
	<fieldset id="office_set" title="">
		<legend>Пункт выдачи карты</legend>
		<div class="element" id="region_div">
			<label for="region">Область/Край<em>*</em></label>
			<select class="required select_noautowidth" name="region" id="region">
			</select>
			<div class="hint" id="region_hint" style="display: none;"><span>Укажите регион</span></div>
		</div>
		<div class="element" id="city_div">
			<label for="city">Населенный пункт<em>*</em></label>
			<select class="required select_noautowidth" name="city" id="city">
				<option val="null">-не выбрано-</option>
			</select>
			<div class="hint" id="city_hint"><span>Укажите населенный пункт</span></div>
		</div>
		<div class="element" id="code_orgunit_div">
			<label for="code_orgunit">Офис<em>*</em></label>
			<select class="required select_noautowidth" name="code_orgunit" id="code_orgunit">
				<option val="null">-не выбрано-</option>
			</select>
			<div class="hint" id="code_orgunit_hint"><span>Укажите офис выдачи</span></div>
		</div>
	</fieldset>
	<input type="hidden" name="clBranchName" id="clBranchName" value="" />
	<fieldset id="personal">
		<legend>Дополнительные услуги</legend>
		<div class="element" id="iBank">
			<label for="personal-data-1-internetBankType">Тип Интернет-банка<em>*</em><img src="img/nb.png" title="Что это?" class="ask"><div class="wtalb" id="wtalb-grace" style="top: -205px;width: 335px;">
                                <span class="wtalbal"  style="top:auto;bottom:-11px;background-position:0 -232px;"></span>
                                <span class="wtalbar" style="top:auto;bottom:-11px;background-position: -50px -232px;"></span>
                                    <div class="wtalbc ">
                                        <p style="margin:-7px 0 2px;text-align:right;"><img class="closeI" src="img/close_icon.gif" alt="Закрыть" title="Закрыть"></p>
                                        <p style="margin:0 0 5px ;">
										В информационном «Интернет-Банке» доступны только нефинансовые операции (нет возможности совершать платежи, 
										переводы и выпуск виртуальной карты). В полном «Интернет-Банке» доступно проведение как нефинансовых, 
										так и финансовых операций. 
										Полный список операций приведен в 
										<a target="_blank" href="https://www.centrinvest.ru/files/services/fiz/pdf/tb_instr_cib5.pdf">Руководстве пользователя.</a>
                                        </p>
                                        <p style="margin:0 0 5px ;" class="closeI">Закрыть</p>
                                    </div>
                            </div></label>
			<div class="radiogroup">
			    <div>
			    	<input type="radio" id="internetBankTypeFull" name="personal-data-1-internetBankType" value="full" class="required niceRadio"  <?if (isset($_SESSION['indiv.design']['personal-data-1-internetBankType']) &&  $_SESSION['indiv.design']['personal-data-1-internetBankType']=="full"):?>checked="checked"<?endif?>>
			    	<label for="internetBankTypeFull">полный</label>
					<input type="radio" id="internetBankTypInfo" name="personal-data-1-internetBankType" value="info" class="required niceRadio"  <?if (isset($_SESSION['indiv.design']['personal-data-1-internetBankType']) &&  $_SESSION['indiv.design']['personal-data-1-internetBankType']=="info"):?>checked="checked"<?endif?>>
					<label for="internetBankTypeInfo">информационный</label>
				</div>
			</div>
		</div>
		<div class="element">
			<label for="personal-data-1-sms">SMS-уведомление</label>
			<div class="radiogroup">
			    <div>
			    	<input type="radio" id="smsYes" name="personal-data-1-sms" value="yes" class="niceRadio"  <?if (isset($_SESSION['indiv.design']['personal-data-1-sms']) &&  $_SESSION['indiv.design']['personal-data-1-sms']=="yes"):?>checked="checked"<?endif?>>
			    	<label for="smsYes">да</label>
					<input type="radio" id="smsNo" name="personal-data-1-sms" value="no" class="niceRadio"  <?if (isset($_SESSION['indiv.design']['personal-data-1-sms']) &&  $_SESSION['indiv.design']['personal-data-1-sms']=="no"):?>checked="checked"<?endif?>>
					<label for="smsNo">нет</label>
				</div>
			</div>
		</div>
		<?php if ($_SESSION['indiv.design']['chcard']=='individual'){?>
		<div class="element" id="emailEveryMonth">
			<label for="personal-data-1-emailEveryMonth">Ежемесячное получение выписки по карточному счету на e-mail</label>
			<div class="radiogroup">
			    <div>
			    	<input type="radio" id="emailYes" name="personal-data-1-emailEveryMonth" value="yes" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['personal-data-1-emailEveryMonth']) &&  $_SESSION['indiv.design']['personal-data-1-emailEveryMonth']=="yes"):?>checked="checked"<?endif?>>
			    	<label for="emailYes">да</label>
					<input type="radio" id="emailNo" name="personal-data-1-emailEveryMonth" value="no" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['personal-data-1-emailEveryMonth']) &&  $_SESSION['indiv.design']['personal-data-1-emailEveryMonth']=="no"):?>checked="checked"<?endif?>>
					<label for="emailNo">нет</label>
				</div>
			</div>
		</div>
		<div class="element" id="personalDataEmail">
			<label for="personal-data-1-emailAddr">Адрес e-mail</label>
			<input type="text" name="personal-data-1-emailAddr" class="required noncyrilic email" id="personal-data-1-emailAddr" value="<?php if ( isset($_SESSION['indiv.design']['personal-data-1-emailAddr'])) echo $_SESSION['indiv.design']['personal-data-1-emailAddr'];?>" />
			<div id="personal-data-1-emailAddrError" class="hint"><span>Укажите корректный адрес</span></div>
		</div>
		<?php }?>
	</fieldset>
	<div class="clear"></div>
	<input type="button" id="goback"  value="Назад" />
	<input type="submit" name="submit1" id="submitit" value="Далее" />
</form>
