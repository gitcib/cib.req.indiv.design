<?php
   include './phpThumb/phpthumb.class.php';
   function listPicsFromCat($catname){
   		$phpThumb = new phpThumb();
	   	$output="";
	   	$gallery = glob(dirname(__FILE__)."/images/custom.design/".$catname."/*.jpg");
	   	foreach ($gallery as $imagefname) {
	   		$basename = basename($imagefname, ".jpg");
	   		$src='images/custom.design/'.$catname.'/'.$basename.'.jpg';
	   		$dst='images/custom.design/'.$catname.'/thumbs/'.$basename.'_thumb.jpg';
	   		if (!file_exists('./'.$dst)){
		   		$phpThumb->resetObject();  		
		   		$phpThumb->setParameter('w', 180);
		   		$phpThumb->setParameter('h', 120);
		   		$phpThumb->setParameter('q', 100);
		   		$phpThumb->setParameter('zc', 1);
		   		$phpThumb->setSourceFilename(dirname(__FILE__).'/images/custom.design/'.$catname.'/'.$basename.'.jpg');
		   		$phpThumb->GenerateThumbnail();
		   		$phpThumb->renderToFile(dirname(__FILE__).'/images/custom.design/'.$catname.'/thumbs/'.$basename.'_thumb.jpg');
	   		}
	   		$output.="<li><img src='".$dst."'/></li>";
	   		$phpThumb->purgeTempFiles();
	   	}
	   	return $output;
   }
   unset($_SESSION['indiv.design']['edited']); 
   unset($_SESSION['indiv.design']['x1']);
   unset($_SESSION['indiv.design']['x2']);
   unset($_SESSION['indiv.design']['y1']);
   unset($_SESSION['indiv.design']['y2']);
   unset($_SESSION['indiv.design']['w']);
   unset($_SESSION['indiv.design']['h']);
?>
<script src="js/jquery.ui.widget.js"></script>
<script src="js/jquery.iframe-transport.js"></script>
<script src="js/jquery.fileupload.js"></script>
<script src="js/jquery.fileupload-process.js"></script>
<script src="js/jquery.fileupload-validate.js"></script>
<script type="text/javascript">
$(function () {
    $('#fileupload').fileupload({
       // maxFileSize: 5000000,
       // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        autoUpload: true,
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator && navigator.userAgent),
        imageMinWidth: 720,
        imageMinHeight: 461,
        dataType: 'json',
        imageCrop: true, // Force cropped images
        add: function (e, data) {
        	var uploadErrors = [];
            var acceptFileTypes = /^image\/(jpe?g)$/i;
            var goUpload = true;
            var uploadFile = data.files[0];
            if (!(/\.(jpg|jpeg)$/i).test(uploadFile.name)) {
                alert('JPG only');
                goUpload = false;
            }
            //alert(uploadFile.name);
            if (uploadFile.size > 5000000) { // 5mb
            	alert('Please upload a smaller image, max size is 5 MB');
                goUpload = false;
            }
            if (uploadFile.width > 720) { // 5mb
            	alert('Please upload a bigger image, max size is 5 MB');
                goUpload = false;
            }
            if (goUpload == true) {
           	$('#userimage').val(1);
           	$('#file').val(uploadFile.name);
               data.submit();
             //   $("#custom-design-selector-form").submit();
            }
        },
        done: function (e, data) {
       // 	alert(data);
            var res = data.result;
        	
        	$('#file').val(res.files[0].name);
        	$("#submitit").click();
        },
        fail: function(e, data) {
        	  alert('Fail!');
        }/*,
	     progressall: function (e, data) {
	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            $('#progress .bar').css(
	                'width',
	                progress + '%'
	            );
	    	 var uploadFile = data.files[0];
	    		$('#file').val(uploadFile.name);
	     }*/
    });
});
<!--
$(document).ready(function(){
	//window.parent.location.reload();
	window.parent.scrollTo(0, 0);
	$('input#goback').click(function(){
		$('input[name=action]').val($('input[name=prev_action]').val());
		$('input[name=isback]').val("1");
	});
	$('div#submit-button').hide();
	$('.imagesCategory img').click(function(){
		$('.imagesCategory li').css('border', '2px solid #fff');
		$(this).parent().css({'border':'2px solid #50b848'});		
		$('#file').val($(this).attr('src'));
		$('form#custom-design-selector-form input[name=cat]').val($(this).parents(".imagesCategory").attr('id'));
		$('input[name=userimage]').val("0");
		$("#submitit").click();
	});
	$("#catsList li").click(function(){
		if ($(this).hasClass("active")) return;
		$("#catsList li").removeClass("active");
		$(this).addClass("active");
		var cat=$(this).attr("id");
		cat=cat.split("_")[0];
		$(".imagesCategory").children(".catLabel").hide();
		$(".imagesCategory").children("ul").hide();
		$("#"+cat).find(".catLabel").show();
		$("#"+cat).find("ul").show();
	});
	$('form#custom-design-selector-form').submit(function(){
		if ($('form#custom-design-selector-form input[name=isback]').val() != "1") {
			if ($('form#custom-design-selector-form input[name="file"]').val() == "") {
				alert("Необходимо выбрать изображение");
				return false;
			}
		}
	});
	//$("#animals_cat").click();
});
$(window.parent).load(function(){
		
});
//-->
</script>
<form name="custom-design-selector-form" id="custom-design-selector-form"  action="./" method="post">
	<input type="hidden" name="userimage" value="<?php echo isset($_SESSION['indiv.design']['userimage'])?$_SESSION['indiv.design']['userimage']:'';?>" id="userimage"/>
	<input type="hidden" name="prev_action" value="<?php echo $prev_action; ?>" />
	<input type="hidden" name="isback" value="0" />
	<input type="hidden" name="action" value="custom-design-selector" />
	<input type="hidden" name="file" value="<?php echo isset($_SESSION['indiv.design']['file'])?$_SESSION['indiv.design']['file']:'';?>" id="file"/>
	<input type="hidden" name="cat" value="<?php echo isset($_SESSION['indiv.design']['cat'])?$_SESSION['indiv.design']['cat']:'';?>" />
	<fieldset>
		<legend>Выбор картинки</legend>
		<p>Загрузите собственное изображение или выберите из галереи дизайнов.</p>
		<small>Не более 5 Мбайт, формат *.jpg, *.jpeg</small><br />
	</fieldset>
	<div class="yellow-button" id="fileatt">
    	<div id="fileAttTit">Выбрать своё изображение</div>
    	<input data-url="images/user.images/" type="file" style="font-size: 20px; top: 0px; position: absolute; left: 0px; opacity: 0; height: 38px; width: 250px; cursor: pointer;" id="fileupload" name="files[]" multiple>
    </div>	
	<div id="upload"></div>
	<?php 
		$phpThumb = new phpThumb();
		$phpThumb->setParameter('w', 180);
		$phpThumb->setParameter('h', 120);
	?>
	<div id="allpics" class="card-images-grouper">
		<div id="catsList">
			<p class="catLabel">Показать</p>
			<ul>
				<li id="allpics_cat">все</li>
				<li id="cities_cat">города</li>
				<li id="textures_cat">текстуры</li>
				<li id="retro_cat">ретро и сепия</li>
				<li id="nature_cat">природа</li>
				<li id="trips_cat">путешествия</li>
				<li id="art_cat">искусство</li>
				<li id="animals_cat">животные</li>
			</ul>
		</div>
		<div class="imagesCategory" id="cities">
			<p class="catLabel">Города</p>
			<ul><? echo listPicsFromCat('cities');?></ul>
		</div>
		<div class="imagesCategory" id="textures">
			<p class="catLabel">Текстуры</p>
			<ul><? echo listPicsFromCat('textures');?></ul>
		</div>
		<div class="imagesCategory" id="retro">
			<p class="catLabel">Ретро и сепия</p>
			<ul><? echo listPicsFromCat('retro');?></ul>
		</div>
		<div class="imagesCategory" id="nature">
			<p class="catLabel">Природа</p>
			<ul><? echo listPicsFromCat('nature');?></ul>
		</div>
		<div class="imagesCategory" id="trips">
			<p class="catLabel">Путешествия</p>
			<ul><? echo listPicsFromCat('trips');?></ul>
		</div>
		<div class="imagesCategory" id="art">
			<p class="catLabel">Искусство</p>
			<ul><? echo listPicsFromCat('art');?></ul>
		</div>
		<div class="imagesCategory" id="animals">
			<p class="catLabel">Животные</p>
			<ul><? echo listPicsFromCat('animals');?></ul>
		</div>
	</div>
	<div class="clear"></div>
	<input type="submit" name="submit" id="goback" value="Назад" />
	<input type="submit" name="submit" id="submitit" value="Далее" />			
</form>
