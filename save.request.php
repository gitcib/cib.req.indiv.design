<?php
if (file_exists($PATH_counter_f)) {
	$fcnt = fopen($PATH_counter_f, "r");
} else {
	$fcnt = fopen($PATH_counter_f, "w+");
}
function rus2translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '',  'ы' => 'y',   'ъ' => '',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',       
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        '_' => '_',
    );
    return strtr($string, $converter);
}
if ($fcnt) {
	$next_number = ((int) fgets($fcnt)) + 1;
	fclose($fcnt);
	$fcnt = fopen($PATH_counter_f, "w+");
	fwrite($fcnt, $next_number."\r\n");
	fclose($fcnt);	

	$x1 = $_SESSION['indiv.design']['x1'];
	$y1 = $_SESSION['indiv.design']['y1'];
	$w = $_SESSION['indiv.design']['w'];
	$h = $_SESSION['indiv.design']['h'];	
		
		
	if ($_SESSION['indiv.design']['userimage'] == 0) {	
		unlink($_SESSION['indiv.design']['imgfilename']);
		$pic_name=explode("_",$_SESSION['indiv.design']['file']);
		$pic_name=$pic_name[0];
		$pic_name=explode("/",$pic_name);
		$pic_name=array_reverse($pic_name);
		$pic_name=$pic_name[0];
		$cat_name=explode("thumbs",$_SESSION['indiv.design']['file']);
		$cat_name=$cat_name[0];
		$cat_name=explode("/",$cat_name);
		$cat_name=array_reverse($cat_name);
		$cat_name=$cat_name[1];
		$imgfile = dirname(__FILE__)."/images/custom.design/".$cat_name."/".$pic_name.".jpg";
		$ratio = $_SESSION['indiv.design']['cdratio'];
		$x1 = $x1 * $ratio;
		$y1 = $y1 * $ratio;
		$w = $w * $ratio;
		$h = $h * $ratio;
	} else {
		$imgfile = dirname(__FILE__)."/images/user.images/files/".$_SESSION['indiv.design']['file'];
		$ratio = $_SESSION['indiv.design']['cdratio'];
		$x1 = $x1 * $ratio;
		$y1 = $y1 * $ratio;
		$w = $w * $ratio;
		$h = $h * $ratio;
	}

	$card_w = 1016;
	$card_h = 650;
	
	$logopos = $_SESSION['indiv.design']['logopos'];
	$logocol = $_SESSION['indiv.design']['logocol'];
	$cardClass = $_SESSION['indiv.design']['class'];
	
	switch ($cardClass) {
		case "Visa Electron":
			$cardClass = "visa_e";
			break;
		case "Visa Classic":
			$cardClass = "visa";
			break;
		case "MIR":
			$cardClass = "mir";
			break;
		default:
			$cardClass = "visa_e";
			break;
	}	
	//var_dump($logopos);
	//echo "<br />";
	//var_dump($logocol);
	//echo "<br />";
	$logofname = "";
	switch ($logocol) {
		case "green":
			$logofname = "Logo_g_498x104.png";
			break;
		case "yellow":
			$logofname = "Logo_y_498x104.png";
			break;
		case "white":
			$logofname = "Logo_w_498x104.png";
			break;
		default:
			$logofname = "";
			break;
	}
	$logo_x_pos = 0;
	$logo_y_pos = 0;
	switch ($logopos) {
		case "left":
			$logo_x_pos = 50;
			$logo_y_pos = 50;
			break;
		case "right":
			$logo_x_pos = $card_w - 498 - 50;
			$logo_y_pos = 50;
			break;
		default:
			$logo_x_pos = 0;
			$logo_y_pos = 0;
			break;
	}
		
	$src_image = imagecreatefromjpeg($imgfile);

	$new_image = imagecreatetruecolor($card_w, $card_h);
	
	imagecopyresampled($new_image, $src_image, 0, 0, $x1, $y1, $card_w, $card_h, $w, $h);
    $add_path_name = "_". $_SESSION['indiv.design']['clLastname'] . "_" . $_SESSION['indiv.design']['clName'] . "_" . $_SESSION['indiv.design']['clSurname'];
    $add_path_name = rus2translit($add_path_name); 
    $add_path_name = preg_replace('/\s/', '', $add_path_name);
	$send_img_name = "indiv_".date("dmY")."_".$next_number.$add_path_name.".jpg";
	if (!empty($logofname)) {
		$logoimg = imagecreatefrompng(dirname(__FILE__)."/images/logos/".$logofname);
		imagecopy($new_image, $logoimg, $logo_x_pos, $logo_y_pos, 0, 0, 498, 104);
	}

	imagejpeg($new_image, '/mnt/big/binkd/out/'.$send_img_name,100);
	$path = '/mnt/big/binkd/out/'.$send_img_name;
	$image = file_get_contents($path);
	$image = substr_replace($image, pack("cnn", 1, 300, 300), 13, 5);
	$f = fopen($path, "w");
	fwrite($f, $image);
	fclose($f);
    //copy for test do 10.10.2014
    $f = fopen(dirname(__FILE__)."/images/custom.design/".$send_img_name, "w");
	fwrite($f, $image);
	fclose($f);

	$send_txt_name = "indiv_".date("dmY")."_".$next_number.$add_path_name.".txt";
	$f_txt = fopen('/mnt/big/binkd/out/'.$send_txt_name, "w+");

	fwrite($f_txt, "begin:\r\n");
	fwrite($f_txt, "clName:".$_SESSION["indiv.design"]["clName"]."\r\n");
	fwrite($f_txt, "clSurname:".$_SESSION["indiv.design"]["clSurname"]."\r\n");
	fwrite($f_txt, "clLastname:".$_SESSION["indiv.design"]["clLastname"]."\r\n");
	fwrite($f_txt, "clBirthDate:".$_SESSION["indiv.design"]["clBirthDate"]."\r\n");	
	if (isset($_SESSION["indiv.design"]["clBirthPlace"])) fwrite($f_txt, "clBirthPlace:".$_SESSION["indiv.design"]["clBirthPlace"]."\r\n");
	if (isset($_SESSION["indiv.design"]["clPassportSeries"])) fwrite($f_txt, "clPassportSeries:".$_SESSION["indiv.design"]["clPassportSeries"]."\r\n");
	if (isset($_SESSION["indiv.design"]["clPassportNum"])) fwrite($f_txt, "clPassportNum:".$_SESSION["indiv.design"]["clPassportNum"]."\r\n");
	if (isset($_SESSION["indiv.design"]["clPassportIssuePlace"])) fwrite($f_txt, "clPassportIssuePlace:".$_SESSION["indiv.design"]["clPassportIssuePlace"]."\r\n");
	if (isset($_SESSION["indiv.design"]["clPassportIssueDate"])) fwrite($f_txt, "clPassportIssueDate:".$_SESSION["indiv.design"]["clPassportIssueDate"]."\r\n");
	if (isset($_SESSION["indiv.design"]["clDivisionCode"])) fwrite($f_txt, "clDivisionCode:".$_SESSION["indiv.design"]["clDivisionCode"]."\r\n");
	if (isset($_SESSION["indiv.design"]["personal-data-1-rfResident"])) fwrite($f_txt, "clResidentRf:".$_SESSION["indiv.design"]["personal-data-1-rfResident"]."\r\n");
	if (isset($_SESSION["indiv.design"]["clINN"])) fwrite($f_txt, "clINN:".$_SESSION["indiv.design"]["clINN"]."\r\n");
	if (isset($_SESSION["indiv.design"]["clRegAddress"])) fwrite($f_txt, "clRegAddress:".$_SESSION["indiv.design"]["clRegAddress"]."\r\n");
	fwrite($f_txt, "clCellPhoneNum:".$_SESSION["indiv.design"]["clCellPhoneNum"]."\r\n");
	if (isset($_SESSION["indiv.design"]["clCodeWord"])) fwrite($f_txt, "clCodeWord:".$_SESSION["indiv.design"]["clCodeWord"]."\r\n");
	if (isset($_SESSION["indiv.design"]["personal-data-1-workPlace"])) fwrite($f_txt, "clWorkPlace:".$_SESSION["indiv.design"]["personal-data-1-workPlace"]."\r\n");
	if (isset($_SESSION["indiv.design"]["personal-data-1-workAddress"])) fwrite($f_txt, "clWorkAddress:".$_SESSION["indiv.design"]["personal-data-1-workAddress"]."\r\n");
	if (isset($_SESSION["indiv.design"]["personal-data-1-workPosition"])) fwrite($f_txt, "clWorkPosition:".$_SESSION["indiv.design"]["personal-data-1-workPosition"]."\r\n");
	if (isset($_SESSION["indiv.design"]["personal-data-1-cardCurrency"])) fwrite($f_txt, "clCardCurrency:".$_SESSION["indiv.design"]["personal-data-1-cardCurrency"]."\r\n");
	fwrite($f_txt, "clBranchName:".$_SESSION["indiv.design"]["off_reg"]." ".$_SESSION["indiv.design"]["off_city"]." ".$_SESSION["indiv.design"]["off_addr"]."\r\n");
	if (isset($_SESSION["indiv.design"]["clFirst4numcard"])) fwrite($f_txt, "clFirst4numcard:".$_SESSION["indiv.design"]["clFirst4numcard"]."\r\n");
	if (isset($_SESSION["indiv.design"]["clLast4numcard"])) fwrite($f_txt, "clLast4numcard:".$_SESSION["indiv.design"]["clLast4numcard"]."\r\n");	
	if ($_SESSION["indiv.design"]["personal-data-1-internetBankType"] == "full") {
		$ibank_str = "полный";
	} else {
		$ibank_str = "информационный";
	} 
	fwrite($f_txt, "IBankType:".$ibank_str."\r\n");
//	fwrite($f_txt, "clIBankCodeWord:".$_SESSION["indiv.design"]["personal-data-1-internetBankCodeWord"]."\r\n");	
	fwrite($f_txt, "clClasscard:".$_SESSION["indiv.design"]["personal-data-1-classcard"]."\r\n");
	fwrite($f_txt, "clIBankSMS:".$_SESSION["indiv.design"]["personal-data-1-sms"]."\r\n");
	if (isset($_SESSION["indiv.design"]["personal-data-1-emailEveryMonth"])) fwrite($f_txt, "clMonthlyEmail:".$_SESSION["indiv.design"]["personal-data-1-emailEveryMonth"]."\r\n");
	if (isset($_SESSION["indiv.design"]["personal-data-1-emailAddr"])) fwrite($f_txt, "clEmail:".$_SESSION["indiv.design"]["personal-data-1-emailAddr"]."\r\n");
	fwrite($f_txt, "end:\r\n");
	fclose($f_txt);
	echo '<form><fieldset><legend>Поздравляем!</legend><p>'.$_SESSION["indiv.design"]["clName"].", Ваша заявка сохранена под номером ".$next_number.". В течение 5 рабочих дней с Вами свяжется специалист банка и сообщит о дальнейших действиях.</p><p>Телефон информационно-справочной службы +7&nbsp;863&nbsp;200-00-00</p></fieldset>";
	echo "<br/><a href='./' class='button'>Заполнить новую заявку</a></form>";
	
	$share_img = imagecreatetruecolor(280, 179);
	imagecopyresampled($share_img, $src_image, 0, 0, $x1, $y1, 280, 179, $w, $h);
	$card_view_pic = imagecreatefrompng(dirname(__FILE__)."/images/".$cardClass."_".$logocol."_".$logopos.".png");
	imagecopyresampled($share_img, $card_view_pic, 0, 0, 0, 0, 280, 179, 331, 212);
	//imagecopyresampled($share_img, $new_image, 0, 0, 0, 0, 331, 212, $card_w, $card_h);
	$share_name=time();
	
	imagejpeg($share_img, dirname(__FILE__)."/images/tmp/share_".$share_name.".jpg",100);
	echo "<img src='images/tmp/share_".$share_name.".jpg'/><br/><br/>";
?>
<script type="text/javascript"><!--			
document.write(VK.Share.button({url: 'https://www.centrinvest.ru/ru/online-zayavki/karta-s-individualnym-dizajnom',title: 'Банковские карты с индивидуальным дизайном',description: 'Карты с индивидуальным дизайном от банка "Центр-инвест"! Заполните заявку на официальном сайте банка, а в офис приходите уже за картой!',image: 'http://centrinvest.ru/ru/online-zayavki/indiv.design/images/tmp/<?echo "share_".$share_name.".jpg";?>',noparse: true},{type: 'round', text: "Поделиться картинкой с друзьями"}));
--></script>
<div id="twitter-share">
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@CenterInvest1" />
<meta name="twitter:creator" content="@CenterInvest1" />
<meta id="meta-url" name="twitter:url" content="http://centrinvest.cinet.ru/ru/dev/karta-s-individualnym-dizajnom?t=<?php echo $share_name;?>" />
<meta name="twitter:title" content="Банковские карты с индивидуальным дизайном" />
<meta name="twitter:description" content="Заполните он-лайн заявку прямо на сайте, а в офис приходите уже за картой!" />
<meta name="twitter:image:src" content="https://centrinvest.cinet.ru/ru/dev/indiv.design/images/tmp/<?echo "share_".$share_name.".jpg?t=".$share_name;?>" />
		<a href="https://twitter.com/share" class="twitter-share-button" url="javascript:get_short_url(function(short_url) {console.log(short_url);return short_url;});" data-text="Банковские карты с индивидуальным дизайном" data-via="CenterInvest1" data-lang="ru" data-size="large">Твитнуть</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
		
		</div>
<?
	unset($_SESSION);
	session_destroy();
} else {
	echo "Ошибка сохранения заявки. Попробуйте позднее.";
}
