<script type="text/javascript">
$(document).ready(function(){
	$("#submitit").click(function(){	
		if ( typeof(parent.yaCounter4814476) != "undefined" ){
			parent.yaCounter4814476.reachGoal('design_type');
		} 
	});
	$("input[name='chcard']").change(function(){
		if ($(this).is(":checked") && $(this).val()!="salary_account") {
			if($(this).val() == "indiv_addit") {
				$("#choice").show();
				$(".dop-card").show();
				$(".other-card").hide();
				$("#univ").click();
			}
			if($(this).val() == "individual") {
				$("#choice").show();
				$(".dop-card").hide();
				$(".other-card").show();
				$("#univ").click();
			}
			
			if ($(this).val() != "individual") {
				$("#pens-container").hide();
			} else {
				$("#pens-container").show();
			}
		}
		if ($(this).is(":checked") && $(this).val()=="salary_account") {
			$("#choice").hide();
			$("#information").show();
			$("#electron").click();
			$("#visaClassic").hide();
			$("#visaElectron").show();
			$("#mirInf").show();
		}
	}).change();

	$("input[name='card']").change(function(){
		if ($(this).is(":checked")) {
			$("#information").show();
			switch($(this).val()) {
				case "univ": {
					$("#classic").click();
					$("#visaClassic").show();
					$("#visaElectron").hide();
					$("#mirInf").show();
					break;
				}
				case "narod": {
					$("#electron").click();
					$("#visaClassic").hide();
					$("#visaElectron").show();
					$("#mirInf").show();
					break;
				}
				default: {
					$("#mirCard").click();
					$("#visaClassic").hide();
					$("#visaElectron").hide();
					$("#mirInf").show();
					break;
				}
			}
		}
	}).change();
});
</script>
<form name="choice_typecard" id="choice_typecard" action="./" method="post">
	<input type="hidden" name="action" value="choice_typecard" />
	<input type="hidden" name="isback" value="0" />
	<fieldset id="personal">
		<legend>Выбор карты</legend>
		<div class="element">
    		<label for="elem1">Выберите, какую карту Вы хотите выпустить</label>
			<div class="radiogroup">
			    <div>
			    	<input type="radio" id="salaryAcc" name="chcard" value="salary_account" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['chcard']) &&  $_SESSION['indiv.design']['chcard']=="salary_account"):?>checked="checked"<?endif?>>
			    	<label for="salaryAcc"  style="width:auto !important; float:none; margin-left:20px;">дополнительную карту к зарплатному счету</label>
			    	<input type="radio" id="indivAddit" name="chcard" value="indiv_addit" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['chcard']) &&  $_SESSION['indiv.design']['chcard']=="indiv_addit"):?>checked="checked"<?endif?>>
			    	<label for="indivAddit"  style="width:auto !important; float:none; margin-left:20px;">дополнительную карту физического лица</label>
					<input type="radio" id="indivCard" name="chcard" value="individual" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['chcard']) &&  $_SESSION['indiv.design']['chcard']=="individual"):?>checked="checked"<?endif?>>
					<label for="indivCard"  style="width:auto !important; float:none;margin-left:20px;">отдельную карту физического лица</label>					
				</div>
			</div>
		</div>
	</fieldset>
	<fieldset id="choice" style="display:none;">
		<legend>Выбор тарифа</legend>
		<div class="element">
    		<label for="elem1">Тариф</label>
			<div class="radiogroup">
			    <div>			    	
			    	<input type="radio" id="univ" name="card" value="univ" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['card']) &&  $_SESSION['indiv.design']['card']=="univ"):?>checked="checked"<?endif?>>
			    	<label for="univ"  style="width:auto !important; float:none; margin-left:20px;"><a target="_parent" href="/ru/fiz/bankovskie-karty/new-products/univ">«Универсальная карта»</a></label>
					<input type="radio" id="narod" name="card" value="narod" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['card']) &&  $_SESSION['indiv.design']['card']=="narod"):?>checked="checked"<?endif?>>
					<label for="narod"  style="width:auto !important; float:none;margin-left:20px;"><a target="_parent" href="/ru/fiz/bankovskie-karty/new-products/narod">«Народная карта»</a></label>
					<input type="radio" id="univ-mir" name="card" value="univ-mir" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['card']) &&  $_SESSION['indiv.design']['card']=="univ-mir"):?>checked="checked"<?endif?>>
					<label for="univ-mir"  style="width:auto !important; float:none;margin-left:20px;"><a target="_parent" href="/ru/fiz/bankovskie-karty/new-products/mir">«Универсальная карта Мир»</a></label>
					<div class="other-card">
						<input type="radio" id="narod-mir" name="card" value="narod-mir" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['card']) &&  $_SESSION['indiv.design']['card']=="narod-mir"):?>checked="checked"<?endif?>>
					<label for="narod-mir"  style="width:auto !important; float:none;margin-left:20px;"><a target="_parent" href="/ru/fiz/bankovskie-karty/new-products/mir">«Народная карта Мир»</a></label>
					</div>
			    	<div class="dop-card">
			    		<input type="radio" id="pens" name="card" value="pens" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['card']) &&  $_SESSION['indiv.design']['card']=="pens"):?>checked="checked"<?endif?>>
				    	<label for="pens"  style="width:auto !important; float:none; margin-left:20px;"><a target="_parent" href="/ru/fiz/bankovskie-karty/new-products/pens">«Пенсионная карта»</a></label>
				    	<input type="radio" id="prem" name="card" value="prem" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['card']) &&  $_SESSION['indiv.design']['card']=="prem"):?>checked="checked"<?endif?>>
				    	<label for="prem"  style="width:auto !important; float:none; margin-left:20px;"><a target="_parent" href="/ru/fiz/bankovskie-karty/new-products/premium">«Премиальная карта»</a></label>
				    	<input type="radio" id="excl" name="card" value="excl" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['card']) &&  $_SESSION['indiv.design']['card']=="excl"):?>checked="checked"<?endif?>>
				    	<label for="excl"  style="width:auto !important; float:none; margin-left:20px;"><a target="_parent" href="/ru/fiz/bankovskie-karty/new-products/exclusive ">«Эксклюзивная карта»</a></label>
			    	</div>
					<!-- <div id="pens-container">
						<input type="radio" id="pens" name="card" value="pens" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['card']) &&  $_SESSION['indiv.design']['card']=="pens"):?>checked="checked"<?endif?>>
						<label for="narod"  style="width:auto !important; float:none;margin-left:20px;"><a target="_parent" href="/ru/fiz/bankovskie-karty/new-products/pens">«Пенсионная карта»</a></label>
					</div> -->
				</div>
			</div>
		</div>
	</fieldset>
	<fieldset id="information" style="display:none;">
		<legend>Информация о карте</legend>
		<div class="element">
    		<label for="elem1">Класс карты</label>
			<div class="radiogroup">
			    <div>
			    	<div id="visaElectron" style="display:none;">
			    		<input type="radio" id="electron" name="personal-data-1-classcard" value="Visa Electron" class="required niceRadio">
				    	<label for="electron">Visa Electron</label>
			    	</div>
			    	<div id="visaClassic" style="display:none;">
			    		<input type="radio" id="classic" name="personal-data-1-classcard" value="Visa Classic" class="required niceRadio">
						<label for="classic">Visa Rewards (Classic)</label>
			    	</div>
			    	<div id="mirInf" style="display:none;">
						<input type="radio" id="mirCard" name="personal-data-1-classcard" value="MIR" class="required niceRadio">
						<label for="mir">МИР</label>
			    	</div>
				</div>
			</div>
		</div>
	</fieldset>
	<br/>
	<div class="clear"></div>
	<input type="submit" name="submit" id="submitit" value="Далее" />
</form>
