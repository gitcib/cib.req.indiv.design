<?php
    ini_set('session.save_path', '/mnt/big/apache/htdocs.cib.data/indiv.design/htdocscib_sessions/');
    session_start();//echo session_id();
    unset($_SESSION['indiv.design']['cardTarif']);
?>
<fieldset>
		<legend>Опции карты</legend>
		<div class="element">
			<label>Класс карты<em>*</em></label>
			<div class="radiogroup"  id="personal-data-1-classcard">
			    <div>
			    	<!-- input type="radio" id="maestro" name="personal-data-1-classcard" value="maestro" class="required niceRadio" <?php if (isset($_SESSION['indiv.design']['personal-data-1-classcard']) &&  $_SESSION['indiv.design']['personal-data-1-classcard']=="maestro") echo 'checked="checked"'?>>
			    	<label for="maestro">Maestro</label-->
					<input type="radio" id="visa" name="personal-data-1-classcard" value="visa" class="required niceRadio" checked="checked">
					<label for="visa">VISA Electron</label>
				</div>
			</div>
		</div>
		<div class="element">
			<label>Валюта карточного счета</label>
			<div class="radiogroup"  id="personal-data-1-cardCurrency">
			    <div>
			    	<input type="radio" id="RUR" name="personal-data-1-cardCurrency" value="rur" class="required niceRadio" <?php if (!isset($_SESSION['indiv.design']['personal-data-1-cardCurrency']) ||  $_SESSION['indiv.design']['personal-data-1-cardCurrency']=="rur") echo 'checked="checked"'?>>
			    	<label for="electron" style="width:60px !important;">рубли</label>
			    	<input type="radio" id="eur" name="personal-data-1-cardCurrency" value="eur" class="required niceRadio" <?php if (isset($_SESSION['indiv.design']['personal-data-1-cardCurrency']) &&  $_SESSION['indiv.design']['personal-data-1-cardCurrency']=="eur") echo 'checked="checked"'?>>
					<label for="classic" style="width:64px  !important;;">евро</label>
					<input type="radio" id="USD" name="personal-data-1-cardCurrency" value="usd" class="required niceRadio" <?php if (isset($_SESSION['indiv.design']['personal-data-1-cardCurrency']) &&  $_SESSION['indiv.design']['personal-data-1-cardCurrency']=="usd") echo 'checked="checked"'?>>
					<label for="classic" style="width:130px  !important;;">доллары США</label>
				</div>
			</div>
		</div>
</fieldset>
