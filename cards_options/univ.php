<?php
    ini_set('session.save_path', '/mnt/big/apache/htdocs.cib.data/indiv.design/htdocscib_sessions/');
    session_start();//echo session_id();
    unset($_SESSION['indiv.design']['cardTarif']);
?>
<script>
$("#cardTarif").on('click', '.ask', function(){     
    //alert($(this).html());
        $(this).next(".wtalb").show();
    });
$("#cardTarif").on('click', '.closeI', function(){        
        $(this).parents(".wtalb").hide();
});
</script>
<fieldset>
		<legend>Опции карты</legend>
		<div class="element">
			<label>Класс карты<em>*</em></label>
			<div class="radiogroup"  id="personal-data-1-classcard">
			    <div>
			    	<input type="radio" id="master" name="personal-data-1-classcard" value="master" class="required niceRadio" <?php if (isset($_SESSION['indiv.design']['personal-data-1-classcard']) &&  $_SESSION['indiv.design']['personal-data-1-classcard']=="master") echo 'checked="checked"'?>>
			    	<label for="master">MasterCard World</label>
					<input type="radio" id="visa" name="personal-data-1-classcard" value="visa" class="required niceRadio" <?php if (isset($_SESSION['indiv.design']['personal-data-1-classcard']) &&  $_SESSION['indiv.design']['personal-data-1-classcard']=="visa") echo 'checked="checked"'?>>
					<label for="visa">VISA (Classic) Rewards</label>
				</div>
			</div>
		</div>
		<div class="element">
			<label>Тип карты<em>*</em></label>
			<div class="radiogroup"  id="cardTarif">
			    <div>
			    	<input type="radio" id="credit" name="cardTarif" value="credit" class="required niceRadio" <?php if (!isset($_SESSION['indiv.design']['cardTarif']) || $_SESSION['indiv.design']['cardTarif']=="credit") echo 'checked="checked"'?>>
			    	<label for="credit">кредитная линия <img src="img/nb.png" title="Что это?" class="ask"> <div class="wtalb" id="wtalb-credit" style="top: -205px;width: 335px;">
                                <span class="wtalbal"  style="top:auto;bottom:-11px;background-position:0 -232px;"></span>
                                <span class="wtalbar" style="top:auto;bottom:-11px;background-position: -50px -232px;"></span>
                                    <div class="wtalbc ">
                                        <p style="margin:-7px 0 2px;text-align:right;"><img class="closeI" src="img/close_icon.gif" alt="Закрыть" title="Закрыть"></p>
                                        <p style="margin:0 0 5px ;">
										Данный продукт подходит клиентам, которые хотят иметь на всякий случай достаточный крупный запас денежных средств, 
										к которому можно обратиться в случае возникновения неотложных нужд в любое время суток в любой точке мира.<br/>
										Кредитный лимит до 6-ти заработных плат.
                                        </p>
                                        <p style="margin:0 0 5px ;" class="closeI">Закрыть</p>
                                    </div>
                            </div></label><br/>
					<input type="radio" id="grace" name="cardTarif" value="grace" class="required niceRadio" <?php if (isset($_SESSION['indiv.design']['cardTarif']) &&  $_SESSION['indiv.design']['cardTarif']=="grace") echo 'checked="checked"'?>>
					<label for="grace">грейс период <img src="img/nb.png" title="Что это?" class="ask"><div class="wtalb" id="wtalb-grace" style="top: -205px;width: 335px;">
                                <span class="wtalbal"  style="top:auto;bottom:-11px;background-position:0 -232px;"></span>
                                <span class="wtalbar" style="top:auto;bottom:-11px;background-position: -50px -232px;"></span>
                                    <div class="wtalbc ">
                                        <p style="margin:-7px 0 2px;text-align:right;"><img class="closeI" src="img/close_icon.gif" alt="Закрыть" title="Закрыть"></p>
                                        <p style="margin:0 0 5px ;">
										Данный продукт отлично подходит клиентам, которые используют карту при безналичных расчетах для разумной экономии, 
										т,е. при различных акциях, скидках, а также для совершения запланированных покупок, но с отсрочкой по оплате их стоимости, 
										с льготным периодом до 55 дней.<br/>
										Кредитный лимит до 3-х заработных плат.
                                        </p>
                                        <p style="margin:0 0 5px ;" class="closeI">Закрыть</p>
                                    </div>
                            </div></label><br/>
					<input type="radio" id="debеt" name="cardTarif" value="debеt" class="required niceRadio" <?php if (isset($_SESSION['indiv.design']['cardTarif']) &&  $_SESSION['indiv.design']['cardTarif']=="debеt") echo 'checked="checked"'?>>
					<label for="debеt">дебетовая карта</label>
				</div>
			</div>
		</div>
		<div class="element">
			<label>Валюта карточного счета</label>
			<div class="radiogroup"  id="personal-data-1-cardCurrency">
			    <div>
			    	<input type="radio" id="RUR" name="personal-data-1-cardCurrency" value="rur" class="required niceRadio" <?php if (!isset($_SESSION['indiv.design']['personal-data-1-cardCurrency']) || $_SESSION['indiv.design']['personal-data-1-cardCurrency']=="rur") echo 'checked="checked"'?>>
			    	<label for="electron" style="width:60px !important;">рубли</label>
			    	<input type="radio" id="eur" name="personal-data-1-cardCurrency" value="eur" class="required niceRadio" <?php if (isset($_SESSION['indiv.design']['personal-data-1-cardCurrency']) &&  $_SESSION['indiv.design']['personal-data-1-cardCurrency']=="eur") echo 'checked="checked"'?>>
					<label for="classic" style="width:64px  !important;">евро</label>
					<input type="radio" id="USD" name="personal-data-1-cardCurrency" value="usd" class="required niceRadio" <?php if (isset($_SESSION['indiv.design']['personal-data-1-cardCurrency']) &&  $_SESSION['indiv.design']['personal-data-1-cardCurrency']=="usd") echo 'checked="checked"'?>>
					<label for="classic" style="width:130px  !important;">доллары США</label>
				</div>
			</div>
		</div>
</fieldset>