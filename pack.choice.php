<?php if (isset($_SESSION['indiv.design']['chcard']) &&  $_SESSION['indiv.design']['chcard']=="salary_account") unset($_SESSION['indiv.design']['card']);?>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('input[type=submit]').attr('readonly', 'readonly');
	$('#cbAccept').change(function(){
		if (this.value) {
			$('input[type=submit]').removeAttr('readonly');
		} else {
			$('input[type=submit]').attr('readonly', 'readonly');
		}
	});
	$('a.readmore').click(function(){
		var linkId = $(this).attr('id');
		var numId = linkId.replace(/input/g,'');
		var oOffset = $(this).offset();
		$('div#info'+numId).fadeIn('slow');
	});
	$('div.readmore_hidden readmore_hidden_close').click(function(){
		$(this).closest('div.readmore_hidden').fadeOut('slow');
	});
	$('div.readmore_hidden').click(function(){
		$(this).closest('div.readmore_hidden').fadeOut('slow');
	});
	$('form#sa-pack-choice').submit(function(){	
			if ( typeof(parent.yaCounter4814476) != "undefined" ){
				parent.yaCounter4814476.reachGoal('design_accept');
			} 

			if ($('input[name=agree-pers-data]:checked', '#sa-pack-choice').val() != undefined || $('input[name=isback]').val()=="1") { 
			    return true; 
			}
	}); 
	$('input#goback').click(function(){ 
		$('input[name=action]').val($('input[name=prev_action]').val()); 
		$('input[name=isback]').val("1"); 
		$('input[type=submit]').removeAttr('readonly').trigger("click"); 
	});	 	
});
//-->
</script> 
<form name="sa-pack-choice" id="sa-pack-choice" action="./" method="post">
	<input type="hidden" name="action" value="pack_choice" />
	<input type="hidden" name="prev_action" value="<?php echo $prev_action; ?>" />
	<input type="hidden" name="isback" value="0" />
	<fieldset>
		<legend>Информация</legend>
		<?php if ($_SESSION['indiv.design']['chcard']=='salary_account'){?>
			<p>Карта с&nbsp;индивидуальным дизайном будет выпущена в&nbsp;качестве дополнительной карты к&nbsp;Вашему зарплатному счету. Стоимость выпуска и&nbsp;обслуживания 
			составит 199 рублей для карты класса Visa Electron и&nbsp;399 рублей для карты класса Visa Classic. Срок действия карты с&nbsp;индивидуальным дизайном&nbsp;&mdash; 5&nbsp;лет.
			Перевыпуск карты с&nbsp;индивидуальным дизайном осуществляется по&nbsp;отдельной заявке, поданной с&nbsp;сайта банка &laquo;Центр-инвест&raquo;.</p>	   
		<?php } else {?>
			<p>Стоимость выпуска и обслуживания карты определяется финансовыми условиями выбранного Вами продукта. Срок действия карты с&nbsp;индивидуальным дизайном&nbsp;&mdash; 5&nbsp;лет.
		<?php }?>
		<p>При создании индивидуального дизайна Вы можете использовать изображение из <a href="./?gal_preview=1">галереи дизайнов</a>, либо загрузить собственное изображение.</p>
	</fieldset>
	<fieldset>
		<legend>Соглашения</legend>
		<?php if ($_SESSION['indiv.design']['chcard']=='salary_account'){?>
			<p>C <a target="_blank" href="./docs/rules-ind-design.pdf"> Правилами выпуска карты с индивидуальным дизайном</a> и 
			<a target="_blank" href="./docs/ind_design_for_zp.doc">Тарифами</a>
			я ознакомлен и полностью согласен, обязуюсь их соблюдать.</p>  
		<?php } else {?>
			<?php if ($_SESSION['indiv.design']['chcard']=='individual'){?>
			<p>Настоящим выражаю свое согласие на заключение договора об открытии карточного счета и выпуске основной банковской карты путем присоединения к 
			Правилам обслуживания и пользования банковскими  картами  ПАО КБ «Центр-инвест».</p>
			<?php } ?>
			<p>
			С <a target="_blank" href="./docs/card-rules.pdf">Правилами пользования пластиковой картой</a>, 
			<a target="_blank" href="./docs/<?php echo $_SESSION['indiv.design']['card']?>.pdf">Тарифами</a> и 
			<a target="_blank" href="./docs/rules-ind-design.pdf">Правилами выпуска карты с индивидуальным дизайном</a>
			я ознакомлен и полностью согласен, обязуюсь их соблюдать.</p>
			<?php if ($_SESSION['indiv.design']['chcard']=='individual'){?>
			<p>Я понимаю и соглашаюсь с тем, что:</p>
			<ul>
				<li>в случае положительного решения Банка о заключении со мной Договора, Правила и Тарифы будут являться его неотъемлемой частью;</li>
				<li>Банк вправе в одностороннем порядке изменять Правила и Тарифы.</li>
			</ul>
			<?php } ?>
		<?php } ?>
		<input type="checkbox" name="agree-pers-data" value="1" id="cbAccept" class="required" <?if (isset($_SESSION['indiv.design']['agree-pers-data'])):?>checked="checked"<?endif?>>
		<label for="cbAccept" style="display:inline-block;width:auto;float:none;vertical-align:top;">Согласен с передачей данных</label>  
		<a href="javascript:void(0);" class="readmore" style="vertical-align:text-top;" id="input1"><small>подробнее</small></a><br />
		<div class="readmore_hidden hidden" id="info1" style="font-family: Arial; font-size: 14px; border: 2px solid #dedede; background-color: #ffffff;">
    		<a href="#" class="readmore_hidden_close"></a><div class="clear"></div>
    		<i><p>Я, ниже подписавшийся, в целях получения любых услуг, оказываемых физическим лицам ПАО КБ «Центр-инвест» (адрес: 344000 г. Ростов-на-Дону, пр. Соколова, 62) - 
			далее именуемое «Банк», и проведения операций через Банк, в т.ч. связанных с открытием и ведением текущих, вкладных и иных счетов, получением кредитов, 
			предоставлением поручительств, залогов, а также получения информации о продуктах и услугах Банка, даю свое согласие Банку на совершение всех действий с 
			персональными данными, предусмотренных Федеральным законом «О персональных данных» № 152-ФЗ 27.07.06 г. (обработка всеми способами). 
			Обработка осуществляется как с использованием средств автоматизации, так и без использования таких средств. 
			В случае обработки персональных данных другим лицом на основании договора, существенным условием договора должна являться обязанность 
			обеспечения указанным лицом конфиденциальности персональных данных и безопасности персональных данных при их обработке. 
			Согласие дается в отношении моих персональных данных, включающих в себя любую информацию, относящуюся ко мне, в том числе фамилию, 
			имя, отчество, год, месяц, дату и место рождения,  адрес, семейное положение, профессию и любую иную информацию, содержащуюся в документах, 
			представленных мною либо полученную в результате ее проверки, а также в отношении копии всех страниц документа, удостоверяющего личность. 
			Согласие действует в течение всего срока действия Договора, а также в течение 75 лет с даты прекращения обязательств сторон по Договору, 
			либо с даты подачи документов в случае если по каким-либо причинам кредит не будет выдан. Персональные данные подлежат уничтожению по 
			истечении 75 лет с даты прекращения обязательств сторон по заключенному сторонами Договору. Согласие может быть отозвано после прекращения 
			всех обязательств по Договору путем письменного уведомления об этом Банка по юр. адресу за 30 дней до даты отзыва. По истечении 30 дней с даты 
			получения отзыва Банк прекращает обработку персональных данных.</p>
			<p>Я заявляю, что вся предоставленная мною информация является достоверной и полной. Я признаю за Банком право обращаться в любой источник за необходимой информацией. 
			Я признаю за Банком право отклонить мое заявление без каких-либо  устных или письменных объяснений. Я не возражаю получать рекламную рассылку по электронной почте.</p></i>
			<input type='button' value='Закрыть' class="readmore_hidden_close" />
    	</div>
	</fieldset>
	<input type="button" id="goback"  value="Назад" />
	<input type="submit" id="submitit" name="submit" value="Далее" />
</form>
