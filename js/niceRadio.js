function changeRadio(el){
	var el = el,
	//input = el.find("input").eq(0);
	input = el;
	if (input.attr("disabled")=="disabled") return;  /*by ladyjane 28.06.2013*/
	var nm=input.attr("name");	
	jQuery(".niceRadio input").each(function() {
		if(jQuery(this).attr("name")==nm)
		{
			jQuery(this).parent().removeClass("radioChecked");
		}
	});					  
	if(el.attr("class") && el.attr("class").indexOf("niceRadioDisabled")==-1){	
		el.addClass("radioChecked");
		input.attr("checked", true);
	}
	input.change();
	return true;
}

function changeVisualRadio(input){
	//alert("!!!");
	var wrapInput = input.parent();
	var nm=input.attr("name");
	if(input.is(":checked")){
		wrapInput.removeClass("errored");
		input.removeClass("errored");
	}
	/*$(".niceRadio input").each(
		function() {
			if(jQuery(this).attr("name")==nm)
				jQuery(this).parent().removeClass("radioChecked");
	});*/
	if(input.attr("checked")) {
		wrapInput.addClass("radioChecked");
		///HIDEALLBOX
		if(input.is('.hideallbox')){
			wrapInput.parent().siblings('div').addClass('hidden');
		}else if(input.is('.showallbox')){
			wrapInput.parent().siblings('div').removeClass('hidden');
		}	
	}
	else {
		///HIDEALLBOX
		if(input.is('.hideallbox')){
			wrapInput.parent().siblings('div').addClass('hidden');;
		}else if(input.is('.showallbox')){
			wrapInput.parent().siblings('div').removeClass('hidden');;
		}	
	}
}

function changeRadioStart(el){
	try{
		var el = el,
		radioName = el.attr("name"),
		radioId = el.attr("id"),
		radioChecked = el.attr("checked"),
		radioDisabled = el.attr("disabled"),
		radioValue = el.attr("value");		
		/*
		if(radioChecked)
			el.after("<span class='niceRadio radioChecked'><input type='radio'"+"name='"+radioName+"'"+"id='"+radioId+"'"+"checked='"+radioChecked+"'"+"value='"+radioValue+"' /></span>");
		else
			el.after("<span class='niceRadio'>"+"<input type='radio'"+"name='"+radioName+"'"+"id='"+radioId+"'"+"value='"+radioValue+"' /></span>");
		*/
		if(radioChecked)
			el.wrap("<div class='niceRadio radioChecked'></div>");
		else
			el.wrap("<div class='niceRadio'></div>");

		/* если контрол disabled - добавляем соответсвующий класс для нужного вида и добавляем атрибут disabled для вложенного radio */		
		if(radioDisabled)
			el.parent().addClass("niceRadioDisabled");

		/* цепляем обработчики стилизированным radio */
		el.parent().siblings("label").each(function(){
			$(this).click(function(e) { changeRadio($("#"+$(this).attr("for"))) });
		});
		
		el.parent().click(function(e) { /*alert($(this).get(0).tagName);*/ changeRadio($(this).children().eq(0)) });		
		if(!el.is("[id^='obligtyperadio']")){
			//alert(el.attr("id"));
			el.change(function(e) { changeVisualRadio($(this)) });
		}
		if($.browser.msie)
			el.click(function(e) { changeVisualRadio($(this)) });	
		//el.remove();
	}
	catch(e){
		// если ошибка, ничего не делаем
	}
    	return true;
}