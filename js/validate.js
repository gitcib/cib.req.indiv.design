(function($){
	$.fn.setuphints=function(){
		$(this).find(".element").focusin(function(event) {
			$(this).children(".hint").show();
			$(".element").not($(this)).focusout(elementFocusOut).focusout();
			
		});
		

		function elementFocusOut(event){
			$(this).children(".hint").hide();
			//$(this).children("select,input").each(function(){$(this).focusout();});
			if (!$(this).children("select,input").hasClass('errored') && !$(this).children("select,input").val()==""){
				$(this).children("select,input").addClass('valid');
			}
			else 
				$(this).children("select,input").removeClass('valid');
		}
		$('form').trigger("beforesubmit");
		$(".element").focusout(elementFocusOut).focusout();
	}
})($);

//remove required message to show basic hint on empty fields
$.validator.addMethod("required", $.validator.methods.required, "");

$.validator.addMethod("cyrillic", function (value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁйЙ\s]+$/.test(value);
}, "Допустимы только русские буквы");

$.validator.addMethod("noncyrillic", function (value, element) {
    return this.optional(element) || /![а-яА-ЯёЁйЙ]+$/.test(value);
}, "Русские буквы не допустимы");

$.validator.addMethod("cyrillic-lname", function (value, element) {
    return this.optional(element) || /^[\- а-яА-ЯёЁйЙ]+$/.test(value);
}, "Допустимы только русские буквы");

$.validator.addMethod("nonlatin", function (value, element) {
    return this.optional(element) || !(/[a-z]+/i.test(value));
}, "Латинские буквы недопустимы");

$.validator.addMethod("frelate", function (value, element) {
    return this.optional(element) || parseInt(value)>0;
}, "Укажите родственную связь");

$.validator.addMethod("fio", function (value, element) {
    return this.optional(element)|| /^\s*([а-яА-ЯёЁйЙ]+|[а-яА-ЯёЁйЙ]+\-[а-яА-ЯёЁйЙ]+)(\s+([а-яА-ЯёЁйЙ]+|[а-яА-ЯёЁйЙ]+\-[а-яА-ЯёЁйЙ]+))+\s*$/.test(value);
}, "Укажите корректные данные");

$.validator.addMethod("txtreq", function (value, element) {
    return this.optional(element)|| /[а-яёйА-ЯЁЙa-zA-Z]+/.test(value);
}, "Укажите корректные данные");
$.validator.addMethod("pass-ser", function (value, element) {
    return this.optional(element) || /^\d{2} \d{2}$/.test(value);
}, "Укажите серию паспорта");
$.validator.addMethod("pass-num", function (value, element) {
    return this.optional(element) || /^\d{6}$/.test(value);
}, "Укажите номер паспорта");
$.validator.addMethod("pass-kp", function (value, element) {
    return this.optional(element) || /^\d{3}-\d{3}$/.test(value);
}, "Укажите код подразделения");
$.validator.addMethod("phone", function (value, element) {
    return this.optional(element) || /^\+7\d{10}$/.test(value);
}, "Укажите 10 цифр, например +78632000000");
$.validator.addMethod("year", function (value, element) {
    return this.optional(element) || /^\d{4}$/.test(value);
}, "Укажите корректный год");
$.validator.addMethod("year", function (value, element) {
    return this.optional(element) || /^\d{4}$/.test(value);
}, "Укажите корректный год");
$.validator.addMethod("inn", function (value, element) {
    return this.optional(element) || /^\d{12}$/.test(value);
}, "Укажите 12 цифр");
$.validator.addMethod("email", function (value, element) {
    return this.optional(element) || /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
}, "Укажите корректный адрес");

$.validator.addMethod("avarage", function (value, element) {
    return this.optional(element) || /^\d(.\d)?$/.test(value);
}, "Укажите корректное число, например 4.5");

$.validator.addMethod("date", function (value, element) {
    return this.optional(element) || (/^\d{2}.\d{2}.\d{4}$/.test(value) && isvaliddate(value));
}, "Укажите корректную дату");

$.validator.addMethod("birthdate", function (value, element) {
    return this.optional(element) || (/^\d{2}.\d{2}.\d{4}$/.test(value) && isvalidbirthdate(value));
}, "Укажите корректную дату");

function isvaliddate(s){
	 var bits = s.split('.');
	  var d = new Date(bits[2], bits[1] - 1, bits[0]);
	  return d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]);
}
function isvalidbirthdate(s){
	var day = s.split('.')[0];
	var month = s.split('.')[1];
	var year = s.split('.')[2];
	var age = 18;
	 
	var mydate = new Date();
	mydate.setFullYear(year, month-1, day);
	 
	var currdate = new Date();
	currdate.setFullYear(currdate.getFullYear() - age);
	 
	return (currdate - mydate < 0 ? false : true);
}
 $(document).ready(function(){	
	 var validator = $("form").validate({

		errorElement: "span",
		errorPlacement : function(error, element) {			
			var hint =  $(element).parent(".element").find(".hint");			
			if(error.text()!=""){				
				hint.children().hide();
				error.appendTo(hint);
			}
		},
		highlight: function(element, errorClass) {
			var hint =  $(element).closest(".element").find(".hint");
			if($(element).is('[type=radio]')){
				//hint =  $(element).parent().parent().parent().find(".hint");
				$(element).closest(".radiogroup").find('[name='+$(element).attr('name')+']').not('[type=hidden]').each(function(){
					$(this).parent().addClass(errorClass);
					$(this).addClass(errorClass);
				});
			}
			else if($(element).is('[type=checkbox]')){
				//hint =  $(element).parent().parent().find(".hint");
				$(element).parent().addClass(errorClass);
				$(element).addClass(errorClass);
			}			
			else
				$(element).addClass(errorClass);
			
			hint.addClass(errorClass); 			
			if(hint.children('.errored').text()!=""){
				hint.children().hide();
			}
		},
		unhighlight: function(element, errorClass) {
			///Quick and Dirty fix for Mixed Fields
			if( $(element).attr("name") == "code_cur" ||
			    $(element).attr("name") == "code_credtermper"
				/*$(element).attr("name") == "emp_period_year"  ||
				$(element).attr("name") == "emp_period_month" ||
				$(element).attr("name") == "fin_jobtermlast_year" ||
				$(element).attr("name") == "fin_jobtermlast_month"*/ 
				)
				return;
			///
			var hint = $(element).closest(".element").find(".hint");
			if($(element).is('[type=radio]')){
				$(element).closest(".radiogroup").find('[name='+$(element).attr('name')+']').not('[type=hidden]').each(function(){
					$(this).parent().removeClass(errorClass);
					$(this).removeClass(errorClass);
				});
			}
			else if($(element).is('[type=checkbox]')){
				$(element).parent().removeClass(errorClass);
				$(element).removeClass(errorClass);
			}		
			else			
				$(element).removeClass(errorClass);
		    	
		    if(hint.length){
			    hint.removeClass(errorClass);
			    hint.children().show();
		    }
		},
		errorClass: "errored",
		invalidHandler: function(event, validator) {
			if ($("form").find('input[name=isback]').val()=="1") this.submit();
			else alert("Не все обязательные поля заполнены, либо заполнены неверно! \nПожалуйста, проверьте указанные Вами данные перед отправкой. \nВ случае отсутствия информации — поставьте, пожалуйста, прочерки.")
			
		},
		 submitHandler: function(form) {
        // do other stuff for a valid form
			$('input[type=button],input[type=submit]').attr('disabled','disabled').addClass('disabled');
			
			$(form).trigger("beforesubmit");
			this.submit();
		}
   
	});
	$(this).setuphints();	
	
 });
