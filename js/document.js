(function($){
	$.fn.setupmasks =	function (){	  
		 $(this).find("#mobile").mask("+7 (999) 999-99-99", {autoclear: false});
		 $(this).find("#phone").mask("+7 (999) 999-99-99", {autoclear: false});
		 $(this).find(".year").mask("9999", {autoclear: false});
		 $(this).find(".date").mask("99.99.9999", {autoclear: false});
		 return this;
	}
	$.fn.setupinputs = function (){
  	///prevent submit on Enter replace it with TAB instead
	  $(this).find(':input').not('textarea').keypress(function(event){	
		if (event.which == 10 || event.which == 13){
		    event.preventDefault();
		    
		    ///works like a TAB
			var inputs = $(this).parents('form').eq(0).find(':input').not(':hidden');
			var idx = inputs.index(this);
		 
			if (idx == inputs.length - 1) {	/*inputs[0].select()*/} else {
			inputs[idx + 1].focus(); // handles submit buttons
			inputs[idx + 1].select();
			}
		}
	  });
	  
	 ///replace comma with dot in numbers
	  $(this).find(':input.number').bind('change', function(event){			
		$(this).val($(this).val().replace(/,/,'.'));
	  });
	  
	  ///niceRadio plugin
	  $(this).find("input[type='radio']").addClass("niceRadio").each( function() {
			changeRadioStart($(this));
	  });	  
	  $(this).find("input[type='checkbox']").addClass("niceCheck").each( function() {
			var input=$(this);
			changeCheckStart($(this));
			$(this).change();			
	  });
		
		
  }
})(jQuery);

/* SELECTOR */
(function($){

  $.fn.clearSelect = function() { //очистить выбранные поля в select
	  return this.each(function(){
		  if(this.tagName=='SELECT') {
		      this.options.length = 0;
		      $(this).attr('disabled','disabled');
		  }
	  });
  }

  $.fn.fillSelect = function(dataArray, selected) {
	  return this.clearSelect().each(function(){
   
		  if(this.tagName=='SELECT') {
			  var currentSelect = this;
			  if(dataArray.length>1)
				  if($.support.cssFloat) {
					  currentSelect.add(new Option("",""),null);
				  } else {
					  currentSelect.add(new Option("",""));
				  }
			  
			  $.each(dataArray,function(index,data){				  
				  if(data.value==selected) {
                                      
					  var option = new Option(data.text,data.value,false,true);
					  $(currentSelect).selectedIndex=index;				
				  }
				  else
					  var option = new Option(data.text,data.value);
				  
				  if($.support.cssFloat) {
					  currentSelect.add(option,null);
				  } else {
					  currentSelect.add(option);
				  }
			  });
		  }
	  });
  }

})(jQuery);


function popFrom(from, to, to_sel, src, func){
  	var cit = from.val();
  	var curSel = (to_sel==null)?to.val():to_sel;
  	//if(cit==null)return;
  	if(cit.length == 0) {
  		//to.attr('disabled','disabled');  		
  		to.clearSelect();
  	} else {
  		$.getJSON(src,{call:func,param:cit},function(data) { 
  			if(data==null) return;
  			to.fillSelect(data,curSel)
  			if(data.length > 1);
  				to.removeAttr('disabled');
		});
  	
  	}
};

function popSelected(from, to, to_sel, src, func){
  	var cit = from;  	
  	//if(cit==null)return;
  	if(cit.length == 0) {
  		to.attr('disabled','disabled');
  		to.clearSelect();
  	} else {
  		$.getJSON(src,{call:func, param:cit},function(data) { 
  			if(data==null) return;
  			to.fillSelect(data,to_sel)
  			if(data.length > 1);
  				to.removeAttr('disabled');
		});
  		
  	}
};

function fill(to, src, func){
  		$.getJSON(src,{call:func, param:null},function(data) {
  			if(data==null) return;
  			to.fillSelect(data,null);
  			if(data.length > 1);
  				to.removeAttr('disabled');
		});
};
$(document).load(function() {
	$('#twi-shr', window.parent.document).html($("#twitter-share").html());
	$("#twitter-share").remove();
});
function get_short_url(func)
{
	var login = "ladyjane88";
	var api_key = "R_783a57e36af34cb3bba25987dd539267";
	var t=new Date().getTime();
	var long_url = window.location+"?t="+t;
    $.getJSON(
        "http://api.bitly.com/v3/shorten?callback=?", 
        { 
            "format": "json",
            "apiKey": api_key,
            "login": login,
            "longUrl": long_url
        },
        function(response)
        {
            func(response.data.url);
        }
    );
}
$(document).ready(function() {
	$(this).setupmasks();
	$(this).setupinputs();
	
	

//	$(".twitter-share-button").attr("url",get_short_url(function(short_url) {console.log(short_url);return short_url;}));
//	$("#meta-url").attr("content",get_short_url(function(short_url) {console.log(short_url);return short_url;}));
	/*$(".select_noautowidth").ikSelect({
		autoWidth: false,
		ddFullWidth: false
	}); */
	/*$('input[type="submit"]').click(function(){
		var valid=true;
		$('.required').each(function(){
			if ($(this).val()=="") {
				valid=false;
				$(this).addClass("errored");
			}
		});
		if (!valid) alert("Не все обязательные поля заполнены, либо заполнены неверно! \nПожалуйста, проверьте указанные Вами данные перед отправкой.");
		return valid;

	});*/
	
	
});
