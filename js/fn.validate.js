function runumwbasicpunc(formid, inputid) {
	if (!(/^[А-Яа-я0-9-,\.\s]+$/.test($('form#'+formid+' input#'+inputid).val()))) {
		$('form#'+formid+' input#'+inputid).addClass('errored');
		var inputVal = $('form#'+formid+' input#'+inputid).val();
		if (($('form#'+formid+' input#'+inputid).val() == "") || (typeof(inputVal) == 'undefined')) {
			$('div#'+inputid+'Error').html('<span>Поле необходимо заполнить</span>');
		} else {
			$('div#'+inputid+'Error').html('<span>Может содержать только русские буквы, цифры и знаки препинания</span>');
		}
		return false;
	} else {
		$('form#'+formid+' input#'+inputid).removeClass('errored');
		$('div#'+inputid+'Error').html('&nbsp;');
		return true;
	}
}

function reqemail(formid, inputid) {
	if (!(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test($('form#'+formid+' input#'+inputid).val()))) {
		$('form#'+formid+' input#'+inputid).addClass('errored');
		var inputVal = $('form#'+formid+' input#'+inputid).val();
		if (($('form#'+formid+' input#'+inputid).val() == "") || (typeof(inputVal) == 'undefined')) {
			$('div#'+inputid+'Error').html('<span>Поле необходимо заполнить</span>');
		} else {
			$('div#'+inputid+'Error').html('<span>Необходимо ввести корректный адрес e-mail (name@domain.com)</span>');
		}
		return false;
	}
	$('form#'+formid+' input#'+inputid).removeClass('errored');
	$('div#'+inputid+'Error').html('&nbsp;');
	return true;
}

function emptyemail(formid, inputid) {
	var inputVal = $('form#'+formid+' input#'+inputid).val();	
	if ((inputVal != "") || (typeof(inputVal) == 'undefined')) {
		if (!(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test($('form#'+formid+' input#'+inputid).val()))) {
			$('form#'+formid+' input#'+inputid).addClass('errored');
			$('div#'+inputid+'Error').html('<span>Необходимо ввести корректный адрес e-mail (name@domain.com)</span>');
			return false;
		}
	}
	$('form#'+formid+' input#'+inputid).removeClass('errored');
	$('div#'+inputid+'Error').html('&nbsp;');
	return true;
}

function ruletters(formid, inputid) {
	if (!(/^[А-Яа-я]+$/.test($('form#'+formid+' input#'+inputid).val()))) {
		$('form#'+formid+' input#'+inputid).addClass('errored');
		var inputVal = $('form#'+formid+' input#'+inputid).val();
		if (($('form#'+formid+' input#'+inputid).val() == "") || (typeof(inputVal) == 'undefined')) {
			$('div#'+inputid+'Error').html('<span>Поле необходимо заполнить</span>');
		} else {
			$('div#'+inputid+'Error').html('<span>Может содержать только русские буквы</span>');
		}
		return false;
	} else {
		$('form#'+formid+' input#'+inputid).removeClass('errored');
		$('div#'+inputid+'Error').html('&nbsp;');
		return true;
	}
}

function ruwspacewdash(formid, inputid) {
	if (!(/^[А-Яа-я\s-]+$/.test($('form#'+formid+' input#'+inputid).val()))) {
		$('form#'+formid+' input#'+inputid).addClass('errored');
		var inputVal = $('form#'+formid+' input#'+inputid).val();
		if (($('form#'+formid+' input#'+inputid).val() == "") || (typeof(inputVal) == 'undefined')) {
			$('div#'+inputid+'Error').html('<span>Поле необходимо заполнить</span>');
		} else {
			$('div#'+inputid+'Error').html('<span>Поле может содержать только русские буквы, пробел и дефис</span>');
		}
		return false;
	} else {		
		$('form#credit-request input#'+inputid).removeClass('errored');
		$('div#'+inputid+'Error').html('&nbsp;');
		return true;
	}
}

function notempty(formid, inputid) {
	var inputVal = $('form#'+formid+' input#'+inputid).val();	
	
	if (($('form#'+formid+' input#'+inputid).val() == "") || (typeof(inputVal) == 'undefined')) {
		$('form#'+formid+' input#'+inputid).addClass('errored');
		$('div#'+inputid+'Error').html('<span>Поле необходимо заполнить</span>');
		return false;
	}	
	$('form#'+formid+' input#'+inputid).removeClass('errored');
	$('div#'+inputid+'Error').html('&nbsp;');
	return true;
}

function requiredminlen(formid, inputid, minlen, minlenmsg) {
	var inputVal = $('form#'+formid+' input#'+inputid).val();
	
	if (($('form#'+formid+' input#'+inputid).val() == "") || (typeof(inputVal) == 'undefined')) {
		$('form#'+formid+' input#'+inputid).addClass('errored');
		$('div#'+inputid+'Error').html('<span>Поле необходимо заполнить</span>');
		return false;
	} else {
		if ($('form#'+formid+' input#'+inputid).val().length < minlen) {
			$('form#'+formid+' input#'+inputid).addClass('errored');
			$('div#'+inputid+'Error').html("<span>"+minlenmsg+"</span>");
			return false;
		} else {
			$('form#'+formid+' input#'+inputid).removeClass('errored');
			$('div#'+inputid+'Error').html('&nbsp;');
			return true;
		}
	}
}

function requiredeqlen(formid, inputid, eqlen, eqlenmsg) {
	var inputVal = $('form#'+formid+' input#'+inputid).val();
		
	if (($('form#'+formid+' input#'+inputid).val() == "") || (typeof(inputVal) == 'undefined')) {
		$('form#'+formid+' input#'+inputid).addClass('errored');
		$('div#'+inputid+'Error').html('<span>Поле необходимо заполнить</span>');
		return false;
	}
	
	if ($('form#'+formid+' input#'+inputid).val().length != eqlen) {
		$('form#'+formid+' input#'+inputid).addClass('errored');
		$('div#'+inputid+'Error').html("<span>"+eqlenmsg+"</span>");
		return false;
	}
	
	$('form#'+formid+' input#'+inputid).removeClass('errored');
	$('div#'+inputid+'Error').html('&nbsp;');
	return true;
}

function isphonenum(formid, inputid) {
	if ($('form#'+formid+' input#'+inputid).val() == "+7") {
		$('form#'+formid+' input#'+inputid).addClass('errored');
		$('div#'+inputid+'Error').html('<span>Поле необходимо заполнить</span>');
		return false;
	} else {
		if ($('form#'+formid+' input#'+inputid).val().length < 12) {
			$('form#'+formid+' input#'+inputid).addClass('errored');
			$('div#'+inputid+'Error').html("<span>Номер телефона должен содержать 12 знаков. Например +78614544243</span>");
			return false;
		} else {
			$('form#'+formid+' input#'+inputid).removeClass('errored');
			$('div#'+inputid+'Error').html('&nbsp;');
			return true;
		}
	}
}

function ischecked(formid, inputid) {
	if (!$('form#'+formid+' input#'+inputid).is(':checked')) {
		$('form#'+formid+' input#'+inputid).addClass('errored');
		$('div#'+inputid+'Error').html('Необходимо ваше согласие');
		return false;
	} else {
		$('form#'+formid+' input#'+inputid).removeClass('errored');
		$('div#'+inputid+'Error').html('&nbsp;');
		return true;
	}
}

function emptyeqlen(formid, inputid, eqlen, errmsg) {
	var inputVal = $('form#'+formid+' input#'+inputid).val();	
	if (((inputVal != "") || (typeof(inputVal) == 'undefined')) && (inputVal.length < eqlen)) {
		$('div#'+inputid+'Error').html(errmsg);
		return false;
	}
	
	$('form#'+formid+' input#'+inputid).removeClass('errored');
	$('div#'+inputid+'Error').html('&nbsp;');
	return true;
}