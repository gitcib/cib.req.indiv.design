function changeCheck(el){	
	input = el.children("input").eq(0);
	if(input.is(':checked')) {
		input.removeAttr("checked");
	}else
		input.attr("checked","checked")
	//alert('actual');
	input.change();
	return true;
}

function changeVisualCheck(input){
	var wrapInput = input.parent();	
	$(wrapInput).removeClass("boxChecked");
	var valholder = wrapInput.parent().children('#'+input.attr('id').replace('_box',''));
	//alert('visual');
	if(input.is(':checked')) {
		///REMOVE ERROR
		wrapInput.parent().find(".niceCheck").removeClass("errored");
		///SET VISUAL CHECKED
		wrapInput.addClass("boxChecked");
		///CHECKBOX VALUE
		if(input.is('[id$=_resident_box]')){
			valholder.val('1');
		} else
			if(input.is('.inversecheckbox'))
				valholder.val('FALSE');
			else
				valholder.val('TRUE');
		///HIDEALLBOX
		if(input.is('.hideallbox')){
			wrapInput.parent().siblings('div').addClass('hidden');
		}else if(input.is('.showallbox')){
			wrapInput.parent().siblings('div').removeClass('hidden');
		}	
		
		///LOAN IS IN BANK
		if(input.is('.isinbank')){				
			var bank = wrapInput.parent().parent().find('input[name="loan_bnkname[]"]');
			bank.val('Центр-Инвест');
			bank.attr('readonly','readonly');
			var contrnum=wrapInput.parent().parent().find('div[id^="loan_contrnum"]');
			contrnum.val('');
			contrnum.hide();
		}
	} else {
		///CHECKBOX VALUE
		if(input.is('[id$=_resident_box]')){
			valholder.val('0');
		} else
		if(input.is('.inversecheckbox'))
			valholder.val('TRUE');
		else
			valholder.val('FALSE');
		///HIDEALLBOX
		if(input.is('.hideallbox')){
			wrapInput.parent().siblings('div').removeClass('hidden');
		}else if(input.is('.showallbox')){
			wrapInput.parent().siblings('div').addClass('hidden');
		}		
		///LOAN IS IN BANK
		if(input.is('.isinbank')){				
			var bank = wrapInput.parent().parent().find('input[name="loan_bnkname[]"]');
			bank.val('');
			bank.removeAttr('readonly');
			wrapInput.parent().parent().find('div[id^="loan_contrnum"]').show();
		}
	}
}

function changeCheckStart(el){
	try{
		var el = el,
		checkName = el.attr("name"),
		checkId = el.attr("id"),
		checkChecked = el.attr("checked"),
		checkDisabled = el.attr("disabled"),
		checkTab = el.attr("tabindex");
		checkValue = el.attr("value");
		checkClass = el.attr("class");
/*
		if(checkChecked)
			el.after("<span class='"+checkClass+" boxChecked'><input type='checkbox'"+"name='"+checkName+"'"+"id='"+checkId+"'"+"checked='"+checkChecked+"'"+"tabindex='"+checkTab+"'"+"value='"+checkValue+"' class='"+checkClass+"'/></span>");
		else
			el.after("<span class='"+checkClass+"'>"+"<input type='checkbox'"+"name='"+checkName+"'"+"id='"+checkId+"'"+"tabindex='"+checkTab+"'"+"value='"+checkValue+"' class='"+checkClass+"'/></span>");
*/
		if(checkChecked=='checked')
			el.wrap("<div class='"+checkClass+" boxChecked'></div>");
		else
			el.wrap("<div class='"+checkClass+"'></div>");

		/* если контрол disabled - добавляем соответсвующий класс для нужного вида и добавляем атрибут disabled для вложенного check */		
		if(checkDisabled)
		{
			el.parent().addClass("niceCheckDisabled");
			//el.next().find("input").eq(0).attr("disabled","disabled");
		}

		/* цепляем обработчики стилизированным check */
		el.parent().siblings("label").eq(0).click(function(e) { e.preventDefault(); changeCheck($("#"+$(this).attr("for")).parent()) });
		el.parent().click(function(e) { changeCheck($(this)) });		
		el.change(function(e) { changeVisualCheck($(this)) });
		if($.browser.msie)
			el.click(function(e) { changeVisualCheck($(this)) });	
		//el.remove();
	}
	catch(e){
		// если ошибка, ничего не делаем
	}
    	return true;
}
