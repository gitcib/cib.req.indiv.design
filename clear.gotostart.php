<?php
ini_set('session.gc_maxlifetime', 60*60*24);
ini_set('session.save_path', '/mnt/big/apache/htdocs.cib.data/indiv.design/htdocscib_sessions/');
session_start();
unset($_SESSION['indiv.design']);
session_destroy();
header('Location: ./');
