<?php
	//echo var_dump($_SESSION['indiv.design']);
	if ($_SESSION['indiv.design']['userimage'] == 0) {		
		$pic_name=explode("_",$_SESSION['indiv.design']['file']);
		$pic_name=$pic_name[0];
		$pic_name=explode("/",$pic_name);
		$pic_name=array_reverse($pic_name);
		$pic_name=$pic_name[0];
		$cat_name=explode("thumbs",$_SESSION['indiv.design']['file']);
		$cat_name=$cat_name[0];
		$cat_name=explode("/",$cat_name);
		$cat_name=array_reverse($cat_name);
		$cat_name=$cat_name[1];
		$pathinfo = pathinfo(dirname(__FILE__)."/images/custom.design/".$cat_name."/".$pic_name.".jpg");
		$f_ext = $pathinfo['extension'];
		$f_name_part = md5(time());
		$f_tmp_name = "tmp_".$f_name_part.".".$f_ext;
		$f_tmp_path = $PATH_DATA."/images/tmp/".$f_tmp_name;
		// echo "f_tmp_path" . $f_tmp_path;
		$aImgProp = getimagesize(dirname(__FILE__)."/images/custom.design/".$cat_name."/".$pic_name.".jpg");
		$w = $aImgProp[0];
		$h = $aImgProp[1];
		$ratio = $w / 720;
		$tmp_w = 720;
		$tmp_h = (int) $h/$ratio;
		$img_src = imagecreatefromjpeg(dirname(__FILE__)."/images/custom.design/".$cat_name."/".$pic_name.".jpg");
		$img_tmp = imagecreatetruecolor($tmp_w, $tmp_h);
		imagecopyresampled($img_tmp, $img_src, 0, 0, 0, 0, $tmp_w, $tmp_h, $w, $h);
		imagejpeg($img_tmp, $f_tmp_path);
		$img_src = "images/tmp/".$f_tmp_name;
	} else {
		$pic_name=$_SESSION['indiv.design']['file'];
		$pathinfo = pathinfo(dirname(__FILE__)."/images/user.images/files/".$pic_name);
		$f_ext = $pathinfo['extension'];
		$f_name_part = md5(time());
		$f_tmp_name = "tmp_".$f_name_part.".".$f_ext;
		$f_tmp_path = $PATH_DATA."/images/tmp/".$f_tmp_name;
               // echo "f_tmp_path" . $f_tmp_path;
		$aImgProp = getimagesize(dirname(__FILE__)."/images/user.images/files/".$pic_name);
		$w = $aImgProp[0];
		$h = $aImgProp[1];
		$ratio = $w / 720;
		$tmp_w = 720;
		$tmp_h = (int) $h/$ratio;
		$img_src = imagecreatefromjpeg(dirname(__FILE__)."/images/user.images/files/".$pic_name);
		$img_tmp = imagecreatetruecolor($tmp_w, $tmp_h);
		imagecopyresampled($img_tmp, $img_src, 0, 0, 0, 0, $tmp_w, $tmp_h, $w, $h);
		imagejpeg($img_tmp, $f_tmp_path);
		$img_src = "images/tmp/".$f_tmp_name;
	}
	$aImgProp = getimagesize($img_src);
	$w = $aImgProp[0];
	$h = $aImgProp[1];
?>
<script type="text/javascript">
<!--
function preview(img, selection) {
	var imgWidth = img.width;
	if (!selection.width || !selection.height) return;

	var scaleX = 331 / selection.width;
	var scaleY = 212 / selection.height;

	$('#preview img#photo-preview').css({
		width: Math.round(scaleX * <?php echo $w; ?>),
		height: Math.round(scaleY * <?php echo $h; ?>),
		marginLeft: -Math.round(scaleX * selection.x1),
		marginTop: -Math.round(scaleY * selection.y1)
	});
	$('input#edited').val(1);
	$('input#x1').val(selection.x1);
	$('input#y1').val(selection.y1);
	$('input#x2').val(selection.x2);
	$('input#y2').val(selection.y2);
	$('input#w').val(selection.width);
	$('input#h').val(selection.height);
}
$(document).ready(function(){
	window.parent.scrollTo(0, 0);
	$('#goback').click(function(){
		$('input[name=action]').val($('input[name=prev_action]').val());
		$('input[name=isback]').val("1");
	});
	var p_w = $('#photo').width()-20;
	var p_h = $('#photo').height()-20;
	
	//$('#photo').imgAreaSelect( {aspectRatio: '331:212', handles: true, fadeSpeed: 200, minHeight: 212, persistent:true, minWidth: 331, x1: 20, y1: 20, x2: p_w, y2: p_h, onSelectChange: preview} );
	addImgAreaSelect($('#photo'));
	var scaleX = 331 / ($('#x2').val()-$('#x1').val());
	var scaleY = 212 / ($('#y2').val()-$('#y1').val());
	//$("#w").val(selection.width);
	//$("#h").val(Math.round(scaleY * <?php echo $h; ?>));
	$('#preview img#photo-preview').css({
		width: Math.round(scaleX * <?php echo $w; ?>),
		height: Math.round(scaleY * <?php echo $h; ?>),
		marginLeft: -Math.round(scaleX * $('#x1').val()),
		marginTop: -Math.round(scaleY * $('#y1').val())
	});
	
	$('#logo-color input').change(function(){		/*$('select#logo-pos').find("option:selected").val()*/
		$('div#preview img#card-bg').attr("src", "images/"+$('input[name=class]').val()+"/"+$('input[name=color]:checked', '#logo-color').val()+"_"+$('input[name=pos]:checked', '#logo-pos').val()+".png");
		$('form#save-request-form input:[name="logocol"]').val($('input[name=color]:checked', '#logo-color').val());
	}).change();
	$('#logo-pos input').change(function(){		
		$('div#preview img#card-bg').attr("src", "images/"+$('input[name=class]').val()+"/"+$('input[name=color]:checked', '#logo-color').val()+"_"+$('input[name=pos]:checked', '#logo-pos').val()+".png");
		$('form#save-request-form input:[name="logopos"]').val($('input[name=pos]:checked', '#logo-pos').val());
	}).change();
	$("#save-request-form").submit(function(){	
		if ( typeof(parent.yaCounter4814476) != "undefined" ){
				parent.yaCounter4814476.reachGoal('design_select');
		} 
	});
});
function addImgAreaSelect(img){
	var p_w = $('#photo').width()-20;
	var p_h = $('#photo').height()-20;
    var myx1=$('#x1').val();myx2=$('#x2').val();myy1=$('#y1').val();myy2=$('#y2').val();
    // alert($("#edited").val());
   if ($("#edited").val()==0){
                var height = ( $('#photo').width() / 331 ) * 212;
                if( height <= $('#photo').height()){     
                        var diff = ( $('#photo').height() - height ) / 2;
                      //  var coords = { x1 : 0, y1 : diff, x2 : $('#photo').width(), y2 : height + diff };
                        myx1=0;myx2=$('#photo').width();myy1=diff;myy2=height + diff;
                            $("#x1").val(0);
	                    	$("#y1").val(diff);
	                    	$("#x2").val($('#photo').width());
	                    	$("#y2").val(height + diff);
	                    	$("#w").val($('#photo').width());
	                    	$("#h").val(height);

                }   
                else{ // if new height out of bounds, scale width instead
                        var width = ( $('#photo').height() / 212 ) * 331; 
                        var diff = ( $('#photo').width() - width ) / 2;
                    //    var coords = { x1 : diff, y1 : 0, x2 : width + diff, y2: this.height };
                        myx1=diff;myx2=width + diff;myy1=0;myy2=$('#photo').height();
                    //    if (!$('#edited').val()) {
	                        $("#x1").val(diff);
	                    	$("#y1").val(0);
	                    	$("#x2").val(width + diff);
	                    	$("#y2").val($('#photo').height());
	                    	$("#w").val(width);
	                    	$("#h").val($('#photo').height());
                    //    }
                } 
                //alert(1);
                $('#photo').imgAreaSelect({
                    handles : true,
                    aspectRatio : '331:212',
                    fadeSpeed : 200,
                   // show : true,
                    minHeight: 212,
                    persistent:true,
                    x1: myx1, y1: myy1, x2: myx2, y2: myy2,
                    onSelectChange: preview
            });
    }  
   else        {//alert(2);
	    $('#photo').imgAreaSelect({
                    handles : true,
                    aspectRatio : '331:212',
                    fadeSpeed : 200,
                   // show : true,
                    minHeight: 212,
                    persistent:true,
                    x1: myx1, y1: myy1, x2: myx2, y2: myy2,
                    onSelectChange: preview
            });
   }
}

//-->
</script>
<div id="photo-container">
	<img id="photo" src="<?php echo $img_src; ?>" width="<?php echo $w; ?>" height="<?php echo $h; ?>"/>
</div>
<div class="clear"></div>
<form id="save-request-form" name="save-request-form" method="post" action="./">
	<input type="hidden" name="prev_action" value="<?php echo $prev_action; ?>" />
	<input type="hidden" name="isback" value="0" />
	<input type="hidden" id="edited" name="edited" value="<?php echo isset($_SESSION['indiv.design']['edited'])?$_SESSION['indiv.design']['edited']:0;?>" />
	<input type="hidden" name="action" value="custom-design-constructor" />
	<input type="hidden" name="logopos" value="left" />
	<input type="hidden" name="logocol" value="green" />
	<input type="hidden" name="cdratio" value="<?php echo $ratio; ?>" />
	<input type="hidden" name="imgfilename" value="<?php echo $img_src; ?>" />
	<input type="hidden" name="class" value="<?php echo isset($_SESSION['indiv.design']['personal-data-1-classcard'])?$_SESSION['indiv.design']['personal-data-1-classcard']:'Visa Electron';?>" />
	<fieldset>
		<legend>Дизайн карты</legend>
		<div class="element">
			<label>Цвет логотипа банка:</label>
			<div class="radiogroup"  id="logo-color">
			    <div>
			    	<input type="radio" id="green" name="color" value="green" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['color']) &&  $_SESSION['indiv.design']['color']=="green" || !isset($_SESSION['indiv.design']['color'])):?>checked="checked"<?endif?>>
			    	<label for="green" style="width:80px !important;">зеленый</label>
			    	<input type="radio" id="white" name="color" value="white" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['color']) &&  $_SESSION['indiv.design']['color']=="white"):?>checked="checked"<?endif?>>
					<label for="white" style="width:80px  !important;;">белый</label>					
				</div>
			</div>
		</div>
		<div class="element">
			<label>Расположение лого:</label>
			<div class="radiogroup"  id="logo-pos">
			    <div>
			    	<input type="radio" id="left" name="pos" value="left" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['pos']) &&  $_SESSION['indiv.design']['pos']=="left" || !isset($_SESSION['indiv.design']['pos'])):?>checked="checked"<?endif?>>
			    	<label for="left" style="width:80px !important;">слева</label>
			    	<input type="radio" id="white" name="pos" value="right" class="required niceRadio" <?if (isset($_SESSION['indiv.design']['pos']) &&  $_SESSION['indiv.design']['pos']=="right"):?>checked="checked"<?endif?>>
					<label for="right" style="width:80px  !important;;">справа</label>		
				</div>
			</div>
		</div>
	</fieldset>
	<div id="preview">
		<img id="card-bg" src="images/visa_e_green_left.png" border="0" width="331" height="212" />
		<!-- <img id="photo-preview" src="images/custom.design/1_0.jpg" /> -->
		<img id="photo-preview" src="<?php echo $img_src; ?>" />
	</div>
	<div id="selection-data">
		<input type="hidden" name="x1" id="x1" value="<?php echo isset($_SESSION['indiv.design']['x1'])?$_SESSION['indiv.design']['x1']:'0';?>" />&nbsp;<input type="hidden" name="y1" id="y1" value="<?php echo isset($_SESSION['indiv.design']['y1'])?$_SESSION['indiv.design']['y1']:'0';?>" />
		<input type="hidden" name="x2" id="x2" value="<?php echo isset($_SESSION['indiv.design']['x2'])?$_SESSION['indiv.design']['x2']:'331';?>" />&nbsp;<input type="hidden" name="y2" id="y2" value="<?php echo isset($_SESSION['indiv.design']['y2'])?$_SESSION['indiv.design']['y2']:'212';?>" />
		<input type="hidden" name="w" id="w" value="<?php echo isset($_SESSION['indiv.design']['w'])?$_SESSION['indiv.design']['w']:'331';?>" />&nbsp;<input type="hidden" name="h" id="h" value="<?php echo isset($_SESSION['indiv.design']['h'])?$_SESSION['indiv.design']['h']:'212';?>" />
	</div>
	<div class="clear"></div>
	<input type="submit" name="submit" id="goback" value="Назад" />
	<input type="submit" name="submit" id="submitit" value="Далее" />
</form>